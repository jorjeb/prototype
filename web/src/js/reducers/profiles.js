import {
  PROFILES_LOAD, PROFILES_UNLOAD,
  PROFILE_LOAD, PROFILE_UNLOAD,
  PROFILE_ADD, PROFILE_SAVE, PROFILE_FINISH_EDIT,
  PROFILE_META_LOAD, PROFILE_META_UNLOAD,
  PHOTOS_LOAD, PHOTOS_UNLOAD,
  LANGUAGES_LOAD, LANGUAGES_UNLOAD,
  INTERESTS_LOAD, INTERESTS_UNLOAD,
  COUNTRIES_LOAD, COUNTRIES_UNLOAD
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  profiles: {},
  profile: undefined,
  interests: {},
  languages: {},
  countries: {}
};

const handlers = {
  [PROFILES_LOAD]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }
    return { error: action.payload };
  },
  [PROFILES_UNLOAD]: () => initialState,
  [PROFILE_LOAD]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }
    return { error: action.payload };
  },
  [PROFILE_UNLOAD]: () => initialState,
  [PROFILE_ADD]: (state, action) => action.payload,
  [PROFILE_SAVE]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }

    return { ...action.payload };
  },
  [PROFILE_FINISH_EDIT]: (state, action) => action.payload,
  [LANGUAGES_LOAD]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }
    return { error: action.payload };
  },
  [LANGUAGES_UNLOAD]: () => initialState,
  [INTERESTS_LOAD]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }
    return { error: action.payload };
  },
  [INTERESTS_UNLOAD]: () => initialState,
  [COUNTRIES_LOAD]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }
    return { error: action.payload };
  },
  [COUNTRIES_UNLOAD]: () => initialState
};

export default createReducer(initialState, handlers);
