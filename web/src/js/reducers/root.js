import { combineReducers } from 'redux';

import dashboard from './dashboard';
import profiles from './profiles';
import nav from './nav';
import session from './session';

export default combineReducers({
  dashboard,
  nav,
  session,
  profiles
});
