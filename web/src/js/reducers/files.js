import {
  FILE_UPLOAD
} from '../actions';
import { createReducer } from './utils';

const initialState = {
  files: {}
};

const handlers = {
  [FILE_UPLOAD]: (state, action) => {
    if (!action.error) {
      action.payload.error = undefined;
      return action.payload;
    }
    return { error: action.payload };
  }
};

export default createReducer(initialState, handlers);
