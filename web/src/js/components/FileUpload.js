import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Box from 'grommet/components/Box';
import Label from 'grommet/components/Label';
import CloseIcon from 'grommet/components/icons/base/Close';
import FormUploadIcon from 'grommet/components/icons/base/FormUpload';
import Meter from 'grommet/components/Meter';
import Toast from 'grommet/components/Toast';

import Dropzone from 'react-dropzone';

class FileUpload extends Component {
  constructor() {
    super();

    this.state = {
      files: [],
      invalidImageFilesFound: false
    };

    this._onDrop = this._onDrop.bind(this);
    this._onDropRejected = this._onDropRejected.bind(this);
    this._onClose = this._onClose.bind(this);
    this._onRemoveFile = this._onRemoveFile.bind(this);
  }

  _onDrop(files) {
    const { onChange } = this.props;

    if (onChange) {
      onChange(files.slice());
    }

    this.setState({
      files
    });
  }

  _onDropRejected() {
    this.setState({
      invalidImageFilesFound: true
    });
  }

  _onClose() {
    this.setState({
      invalidImageFilesFound: false
    });
  }

  _onRemoveFile(event) {
    const fileName = event.target.getAttribute('data-file-name');

    if (typeof fileName !== 'undefined') {
      const { onChange } = this.props;

      const files = this.state.files.filter(file => file.name != fileName);

      if (onChange) {
        onChange(files.slice());
      }

      this.setState({
        files
      });
    }
  }

  render() {
    const { files } = this.props;

    let filesNode;
    if (this.state.files.length > 0) {
      filesNode = (
        <ul className="dropzone-files" onClick={this._onRemoveFile}>
          {
            this.state.files.map(file => {
              let progressNode;
              if (files.hasOwnProperty(file.name)) {
                progressNode = (
                  <Meter vertical={false}
                    size="xsmall"
                    value={files[file.name].progress}
                  />
                );
              }

              return (<li key={file.name}>
                <span title={file.name}>{file.name}</span>
                <CloseIcon
                  className="remove"
                  size="xsmall"
                  data-file-name={file.name}
                />
                <small>{file.size} bytes</small>
                {progressNode}
              </li>);
            })
          }
        </ul>
      );
    }

    let toastNode;
    if (this.state.invalidImageFilesFound) {
      toastNode = (
        <Toast
          status="critical"
          onClose={this._onClose}
        >
          Invalid image file(s) found.
        </Toast>
      );
    }

    return (
      <Box
        direction="column"
        pad={{ between: 'medium', vertical: 'medium' }}
      >
        {toastNode}
        <div className="dropzone">
          <Dropzone
            accept={this.props.accept}
            onDrop={this._onDrop}
            onDropRejected={this._onDropRejected}
          >
            <p>
              <FormUploadIcon size="large" />
              <strong>Choose a file</strong> or drag it here.
            </p>
          </Dropzone>
        </div>
        {filesNode}
      </Box>
    );
  }
}

FileUpload.defaultProps = {
  accept: '',
  files: [],
  onChange: undefined
};

FileUpload.propTypes = {
  accept: PropTypes.string,
  files: PropTypes.arrayOf(PropTypes.object),
  onChange: PropTypes.func.isRequired
};

const select = state => ({ ...state.files });

export default connect(select)(FileUpload);

