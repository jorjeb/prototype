import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Button from 'grommet/components/Button';
import Carousel from 'grommet/components/Carousel';
import CloseIcon from 'grommet/components/icons/base/Close';
import Footer from 'grommet/components/Footer';
import Form from 'grommet/components/Form';
import FormField from 'grommet/components/FormField';
import FormUploadIcon from 'grommet/components/icons/base/FormUpload';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Image from 'grommet/components/Image';
import Label from 'grommet/components/Label';
import Meter from 'grommet/components/Meter';
import Notification from 'grommet/components/Notification';
import Select from 'grommet/components/Select';
import TextInput from 'grommet/components/TextInput';
import Value from 'grommet/components/Value';
import Spinning from 'grommet/components/icons/Spinning';
import LinkPrevious from 'grommet/components/icons/base/LinkPrevious';

import Textarea from 'react-textarea-autosize';

import _ from 'lodash';

import FileUpload from './FileUpload';
import {
  createDefaultProps,
  createPropTypes,
  getAttributes,
  getProfilePhotoURL,
  pushToKeyedMap
} from '../api/utils';

class ProfileForm extends Component {
  constructor(props) {
    super(props);

    const {
      profileMeta, photos,
      ...profile } = props.profile;

    this.state = {
      profile,
      profileMeta,
      photos,
      formErrors: props.formErrors
    };

    this._onChange = this._onChange.bind(this);
    this._onMetaChange = this._onMetaChange.bind(this);
    this._onFilesChange = this._onFilesChange.bind(this);
    this._onRemovePhoto = this._onRemovePhoto.bind(this);
    this._onSubmit = this._onSubmit.bind(this);
  }

  componentWillReceiveProps(props) {
    const {
      profileMeta, photos,
      ...profile
    } = props.profile;

    this.setState({
      profile,
      profileMeta,
      photos,
      formErrors: props.formErrors
    });
  }

  _onChange(event) {
    const name = event.target.name;
    const value = (typeof event.value !== 'undefined') ?
      event.value :
      event.target.value;

    let profile = { ...this.state.profile, [name]: value };

    this.setState({ ...this.state, profile });
  }

  _onMetaChange(event) {
    const name = event.target.getAttribute('data-name');
    const value = event.target.value;

    let metaIndex = event.target.getAttribute('data-meta-index');
    const languageId = event.target.getAttribute('data-language-id');

    let meta;
    let profileMeta;

    if (metaIndex) {
      meta = { ...this.state.profileMeta[metaIndex], [name]: value };
      profileMeta = { ...this.state.profileMeta, [metaIndex]: meta };
    } else {
      const language = _.find(this.props.languages, v => v.id == languageId);

      meta = {
        [name]: value,
        language
      };

      profileMeta = pushToKeyedMap(this.state.profileMeta, meta);
    }

    this.setState({ ...this.state, profileMeta });
  }

  _onFilesChange(files) {
    let photos = {};

    _.map(this.state.photos, (value, key) => {
      if (value instanceof File) {
        const index = files.findIndex(file => value.name == file.name);
        const file = files.splice(index, 1);

        if (file.length > 0) {
          photos = pushToKeyedMap(photos, file[0]);
        }
      } else if (!value.deleted) {
        photos = pushToKeyedMap(photos, value);
      }
    });

    _.forEach(files, value => {
      photos = pushToKeyedMap(photos, value);
    });

    this.setState({ ...this.state, photos });
  }

  _onRemovePhoto(event) {
    const key = event.currentTarget.getAttribute('data-photo-key');

    const photo = { ...this.state.photos[key], deleted: true };
    const photos = { ...this.state.photos, [key]: photo };

    this.setState({ ...this.state, photos });
  }

  _onSubmit(event) {
    event.preventDefault();

    const { profile, profileMeta, photos } = this.state;
    const { onSubmit } = this.props;

    if (onSubmit) {
      onSubmit({ profile, profileMeta, photos });
    }
  }

  render() {
    const { profile, profileMeta, photos,
            formErrors } = this.state;
    const { languages, interests, countries } = this.props;

    let carouselPhotos = [];

    _.map(photos, (photo, key) => {
      if (!(photo instanceof File) && !photo.deleted) {
        carouselPhotos.push(
          <Box className="carousel-slide" key={`photo_${photo.id}`}>
            <Button
              className="delete-image-button"
              icon={<CloseIcon />}
              plain={true}
              onClick={this._onRemovePhoto}
              data-photo-key={key}
            />
            <Image fit="cover" size="small" src={getProfilePhotoURL(profile, photo)} />
          </Box>
        );
      }
    });

    let metaNodes = {};
    _.forEach(getAttributes('profileMeta'), attr => {
      if (typeof metaNodes[attr] === 'undefined') {
        metaNodes[attr] = [];
      }
    });

    _.map(languages, (language, key) => {
      let index;
      const meta = _.find(profileMeta, (meta, key) => {
        if (meta.language.id == language.id) {
          index = key;
          return true;
        }

        return false;
      });

      _.forOwn(metaNodes, (value, attr) => {
        metaNodes[attr].push(
          <FormField key={`language_${key}`} label={language.name}>
            <Textarea
              data-name={attr}
              data-meta-index={index}
              data-language-id={language.id}
              value={(meta && meta[attr]) ? meta[attr] : ''}
              onChange={this._onMetaChange}
            />
          </FormField>
        );
      });
    });

    return (
      <Form onSubmit={this._onSubmit}>
        <FormField label="Display Name" error={formErrors.displayName ? formErrors.displayName : ''}>
          <TextInput
            name="displayName"
            value={profile.displayName ? profile.displayName : ''}
            onDOMChange={this._onChange}
          />
        </FormField>
        <fieldset className="profile-photos">
          <legend>Photos</legend>
          <Carousel>
            {carouselPhotos}
          </Carousel>
          <FileUpload
            accept="image/jpeg, image/jpg, image/png"
            onChange={this._onFilesChange}
          />
        </fieldset>
        <fieldset className="profile-info">
          <legend>Info</legend>
          <FormField label="Country">
            <Select
              name="country"
              value={(profile.country && profile.country.name) ? profile.country.name : profile.country}
              onChange={this._onChange}
              options={
                _.map(countries, v => v.name)
              }
            />
          </FormField>
          <Box
            direction="row"
          >
            <FormField label="Height">
              <TextInput
                name="height"
                value={profile.height ? profile.height : ''}
                onDOMChange={this._onChange}
              />
            </FormField>
            <FormField label="Weight">
              <TextInput
                name="weight"
                value={profile.weight ? profile.weight : ''}
                onDOMChange={this._onChange}
              />
            </FormField>
          </Box>
          <Box
            direction="row"
          >
            <FormField label="Hair Colour">
              <TextInput
                name="hairColour"
                value={profile.hairColour ? profile.hairColour : ''}
                onDOMChange={this._onChange}
              />
            </FormField>
            <FormField label="Eye Colour">
              <TextInput
                name="eyeColour"
                value={profile.eyeColour ? profile.eyeColour : ''}
                onDOMChange={this._onChange}
              />
            </FormField>
          </Box>
          <Box
            direction="row"
          >
            <FormField label="Body Type">
              <Textarea
                name="bodyType"
                value={profile.bodyType ? profile.bodyType : ''}
                onChange={this._onChange}
              />
            </FormField>
            <FormField label="Body Art">
              <Textarea
                name="bodyArt"
                value={profile.bodyArt ? profile.bodyArt : ''}
                onChange={this._onChange}
              />
            </FormField>
          </Box>
          <Box
            direction="row"
          >
            <FormField label="Gender">
              <TextInput
                name="gender"
                value={profile.gender ? profile.gender : ''}
                onDOMChange={this._onChange}
              />
            </FormField>
            <FormField label="Looking For?">
              <TextInput
                name="lookingFor"
                value={profile.lookingFor ? profile.lookingFor : ''}
                onDOMChange={this._onChange}
              />
            </FormField>
          </Box>
          <FormField label="Orientation">
            <TextInput
              name="orientation"
              value={profile.orientation ? profile.orientation : ''}
              onDOMChange={this._onChange}
            />
          </FormField>
          <FormField label="Ethnicity">
            <TextInput
              name="ethnicity"
              value={profile.ethnicity ? profile.ethnicity : ''}
              onDOMChange={this._onChange}
            />
          </FormField>
          <FormField label="Religion">
            <TextInput
              name="religion"
              value={profile.religion ? profile.religion : ''}
              onDOMChange={this._onChange}
            />
          </FormField>
          <FormField label="Star Sign">
            <Select
              name="starSign"
              value={profile.starSign ? profile.starSign : ''}
              onChange={this._onChange}
              options={[
                'Aquarius',
                'Pisces',
                'Aries',
                'Taurus',
                'Gemini',
                'Cancer',
                'Leo',
                'Virgo',
                'Libra',
                'Scorpio',
                'Sagittarius',
                'Capricorn'
              ]}
            />
          </FormField>
          <FormField label="Status">
            <TextInput
              name="civilStatus"
              value={profile.civilStatus ? profile.civilStatus : ''}
              onDOMChange={this._onChange}
            />
          </FormField>
          <FormField label="Education">
            <TextInput
              name="education"
              value={profile.education ? profile.education : ''}
              onDOMChange={this._onChange}
            />
          </FormField>
          <FormField label="Children">
            <TextInput
              name="children"
              value={profile.children ? profile.children : ''}
              onDOMChange={this._onChange}
            />
          </FormField>
        </fieldset>
        <Box
          direction="column"
          margin={{ vertical: 'medium' }}
        >
          <h3>Blurb</h3>
          { metaNodes.blurb }
        </Box>
        <Box
          direction="column"
          margin={{ vertical: 'medium' }}
        >
          <h3>Living</h3>
          { metaNodes.living }
        </Box>
        <Box
          direction="column"
          margin={{ vertical: 'medium' }}
        >
          <h3>Smoking Habits</h3>
          { metaNodes.smokingHabits }
        </Box>
        <Box
          direction="column"
          margin={{ vertical: 'medium' }}
        >
          <h3>Drinking Habits</h3>
          { metaNodes.drinkingHabits }
        </Box>
        <fieldset>
          <legend>Interests</legend>
        </fieldset>
        <Footer pad={{"vertical": "medium"}}>
          <Box
            direction="row"
            pad={{ between: 'small' }}
          >
            <Button label="Save"
              type="submit"
              primary={true} />
          </Box>
        </Footer>
      </Form>
    );
  }
}

ProfileForm.defaultProps = {
  formErrors: {},
  profile: createDefaultProps('profile'),
  languages: {},
  interests: {},
  countries: {},
  onSubmit: undefined
};

ProfileForm.propTypes = {
  formErrors: PropTypes.object,
  profile: createPropTypes('profile'),
  languages: PropTypes.object,
  interests: PropTypes.object,
  countries: PropTypes.object,
  onSubmit: PropTypes.func.isRequired
};

export default ProfileForm;
