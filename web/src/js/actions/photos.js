import { PHOTOS_LOAD, PHOTOS_UNLOAD } from '../actions';
import { getPhotos } from '../api/photos';

export function loadPhotos(profileId) {
  return async dispatch => {
    try {
      const { photos } = await getPhotos(profileId);

      dispatch({
        type: PHOTOS_LOAD,
        payload: {
          photos
        }
      });
    } catch(error) {
      dispatch({
        type: PHOTOS_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadPhotos() {
  return {
    type: PHOTOS_UNLOAD
  };
}
