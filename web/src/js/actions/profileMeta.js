import { PROFILE_META_LOAD, PROFILE_META_UNLOAD, PROFILE_META_SAVE } from '../actions';
import { getProfileMeta, saveProfileMeta as _saveProfileMeta } from '../api/profileMeta';

export function loadProfileMeta(profileId) {
  return async dispatch => {
    try {
      const { profileMeta } = await getProfileMeta(profileId, ['language']);

      dispatch({
        type: PROFILE_META_LOAD,
        payload: {
          profileMeta
        }
      });
    } catch(error) {
      dispatch({
        type: PROFILE_META_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadProfileMeta() {
  return {
    type: PROFILE_META_UNLOAD
  };
}
