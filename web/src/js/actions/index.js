// See https://github.com/acdlite/flux-standard-action

// Session
export const SESSION_LOAD = 'SESSION_LOAD';
export const SESSION_LOGIN = 'SESSION_LOGIN';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';

// Dashboard
export const DASHBOARD_LOAD = 'DASHBOARD_LOAD';
export const DASHBOARD_UNLOAD = 'DASHBOARD_UNLOAD';

// Profile
export const PROFILE_LOAD = 'PROFILE_LOAD';
export const PROFILE_UNLOAD = 'PROFILE_UNLOAD';
export const PROFILES_LOAD = 'PROFILES_LOAD';
export const PROFILES_UNLOAD = 'PROFILES_UNLOAD';
export const PROFILE_ADD = 'PROFILE_ADD';
export const PROFILE_SAVE = 'PROFILE_SAVE';
export const PROFILE_FINISH_EDIT = 'PROFILE_FINISH_EDIT';

// Interests
export const INTERESTS_LOAD = 'INTERESTS_LOAD';
export const INTERESTS_UNLOAD = 'INTERESTS_UNLOAD';

// Countries
export const COUNTRIES_LOAD = 'COUNTRIES_LOAD';
export const COUNTRIES_UNLOAD = 'COUNTRIES_UNLOAD';

// Languages
export const LANGUAGES_LOAD = 'LANGUAGES_LOAD';
export const LANGUAGES_UNLOAD = 'LANGUAGES_UNLOAD';

// Nav
export const NAV_ACTIVATE = 'NAV_ACTIVATE';
export const NAV_ENABLE = 'NAV_ENABLE';
export const NAV_RESPONSIVE = 'NAV_RESPONSIVE';
