import { DASHBOARD_LOAD, DASHBOARD_UNLOAD } from '../actions';
import { watchDashboard, unwatchDashboard } from '../api/dashboard';

export function loadDashboard() {
  return dispatch => (
    dispatch({ type: DASHBOARD_LOAD, payload: {} })
  );
}

export function unloadDashboard() {
  return { type: DASHBOARD_UNLOAD };
}
