import { INTERESTS_LOAD, INTERESTS_UNLOAD } from '../actions';
import { getInterests } from '../api/interests';

export function loadInterests() {
  return async dispatch => {
    try {
      const { interests } = await getInterests();

      dispatch({
        type: INTERESTS_LOAD,
        payload: {
          interests
        }
      });
    } catch(error) {
      dispatch({
        type: INTERESTS_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadInterests() {
  return {
    type: INTERESTS_UNLOAD
  };
}
