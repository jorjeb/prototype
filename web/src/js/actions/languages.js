import { LANGUAGES_LOAD, LANGUAGES_UNLOAD } from '../actions';
import { getLanguages } from '../api/languages';

export function loadLanguages() {
  return async dispatch => {
    try {
      const { languages } = await getLanguages();

      dispatch({
        type: LANGUAGES_LOAD,
        payload: {
          languages
        }
      });
    } catch(error) {
      dispatch({
        type: LANGUAGES_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadLanguages() {
  return {
    type: LANGUAGES_UNLOAD
  };
}
