export function parseError(error) {
  if (error.response) {
    const { data, status, statusText } = error.response;
    return { data, status, statusText };
  }

  return error;
}
