import NProgress from 'nprogress';

import {
  PROFILES_LOAD, PROFILES_UNLOAD,
  PROFILE_LOAD, PROFILE_UNLOAD,
  PROFILE_ADD,
  PROFILE_SAVE,
  PROFILE_FINISH_EDIT
} from '../actions';
import {
  getProfiles, getProfile, saveProfile as updateProfile
} from '../api/profiles';
import { saveProfileMeta } from '../api/profileMeta';
import { parseError } from './utils';

export function loadProfiles() {
  return async dispatch => {
    try {
      const { profiles } = await getProfiles();

      dispatch({
        type: PROFILES_LOAD,
        payload: { profiles }
      });
    } catch(error) {
      dispatch({
        type: PROFILES_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadProfiles() {
  return {
    type: PROFILES_UNLOAD
  };
}

export function loadProfile(id) {
  return async dispatch => {
    try {
      const { profile } = await getProfile(id, [
        'profile_meta',
        'profile_meta.language',
        'photos',
        'interests',
        'country'
      ]);

      dispatch({
        type: PROFILE_LOAD,
        payload: { profile }
      });
    } catch(error) {
      dispatch({
        type: PROFILE_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadProfile() {
  return {
    type: PROFILE_UNLOAD
  };
}

export function saveProfile(data) {
  return async dispatch => {
    try {
      NProgress.start();

      const { profile } = await updateProfile(data, [
        'profile_meta',
        'profile_meta.language',
        'photos',
        'interests',
        'country'
      ]);

      NProgress.done();

      dispatch({
        type: PROFILE_SAVE,
        payload: {
          profile,
          redirect: true,
          pathname: `/profiles/${profile.id}`,
          saved: true
        }
      });
    } catch(error) {
      console.error(error);

      const profile = {
        profileMeta: data.profileMeta,
        photos: data.photos,
        ...data.profile
      };

      dispatch({
        type: PROFILE_SAVE,
        error: true,
        payload: {
          profile,
          error
        }
      });
    }
  };
}

export function addProfile() {
  return dispatch => {
    dispatch({
      type: PROFILE_ADD,
      payload: {
        redirect: false,
        pathname: '',
        saved: false
      }
    });
  };
}

export function finishProfileEdit() {
  return dispatch => {
    dispatch({
      type: PROFILE_FINISH_EDIT,
      payload: {
        redirect: false,
        pathname: '',
        saved: false
      }
    });
  };
}
