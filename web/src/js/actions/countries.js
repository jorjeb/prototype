import { COUNTRIES_LOAD, COUNTRIES_UNLOAD } from '../actions';
import { getCountries } from '../api/countries';

export function loadCountries() {
  return async dispatch => {
    try {
      const { countries } = await getCountries();

      dispatch({
        type: COUNTRIES_LOAD,
        payload: {
          countries
        }
      });
    } catch(error) {
      dispatch({
        type: COUNTRIES_LOAD,
        error: true,
        payload: error
      });
    }
  };
}

export function unloadCountries() {
  return {
    type: COUNTRIES_UNLOAD
  };
}
