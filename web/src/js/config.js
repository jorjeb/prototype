export default {
  baseURL: 'https://prototype.dev',
  uploadsPath: '/uploads',
  apiPath: '/api/v1',
  storagePath: '/storage/v1'
};
