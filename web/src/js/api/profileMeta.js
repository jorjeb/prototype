import { api } from '../api';

export async function getProfileMeta(profileId, include = []) {
  const response = await api.get(`/profiles/${profileId}/profile-meta`, {
    params: { include }
  });

  return { response, profileMeta: response.data };
}
