import { api } from '../api';

export async function getCountries() {
  const response = await api.get('/countries');

  return { response, countries: response.data };
}
