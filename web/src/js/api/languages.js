import { api } from '../api';

export async function getLanguages() {
  const response = await api.get('/languages');

  return { response, languages: response.data };
}
