import axios from 'axios';
import _ from 'lodash';

import { getIncludeNames, normalized, denormalized } from './utils';
import config from '../config';

let options = {
  // default options
};

const requestFn = axios.Axios.prototype.request;

axios.Axios.prototype.request = function(config) {
  if (config.params) {
    let { include } = config.params;

    if (include && include.length > 0) {
      config.params.include = include.join(',');
    } else {
      delete config.params.include;
    }
  }

  return requestFn.call(this, config);
};

axios.Axios.prototype.map = async function(collection, fn) {
  const promises = _.map(collection, fn);

  return Promise.all(promises);
}

export function create(overrides = {}) {
  return axios.create({ ...options, ...overrides });
};

export const api = create({
  baseURL: `${config.baseURL}${config.apiPath}`,
  headers: {'Content-Type': 'application/vnd.api+json'},
  transformRequest: [(data, headers) => {
    return JSON.stringify(typeof data !== 'undefined' ? denormalized(data) : data);
  }],
  transformResponse: [data => {
    let parsedData = JSON.parse(data ? data : '{}');
    return (!parsedData.errors) ? normalized(parsedData) : parsedData;
  }]
});

export const storage = create({
  baseURL: `${config.baseURL}${config.storagePath}`,
  headers: {'Content-Type': 'multipart/form-data'},
  transformResponse: [data => {
    let parsedData = JSON.parse(data ? data : '{}');
    return (!parsedData.errors) ? normalized(parsedData) : parsedData;
  }]
});

export default { api, storage };