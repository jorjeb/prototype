import { api } from '../api';

export async function getInterests() {
  const response = await api.get('/interests');

  return { response, interests: response.data };
}
