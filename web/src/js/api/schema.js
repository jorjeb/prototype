export default {
  models: {
    profile: {
      bodyArt: { type: 'string' },
      bodyType: { type: 'string' },
      children: { type: 'string' },
      civilStatus: { type: 'string' },
      displayName: { type: 'string', required: true },
      education: { type: 'string' },
      ethnicity: { type: 'string' },
      eyeColour: { type: 'string' },
      gender: { type: 'string' },
      hairColour: { type: 'string' },
      height: { type: 'string' },
      lookingFor: { type: 'string' },
      orientation: { type: 'string' },
      religion: { type: 'string' },
      starSign: { type: 'string' },
      weight: { type: 'string' },
      profileMeta: { type: 'hasMany', model: 'profileMeta' },
      photos: { type: 'hasMany', model: 'photo' },
      interests: { type: 'hasMany', model: 'interest' }
    },
    profileMeta: {
      blurb: { type: 'string' },
      living: { type: 'string' },
      smokingHabits: { type: 'string' },
      drinkingHabits: { type: 'string' },
      language: { type: 'hasOne', model: 'language' }
    },
    photo: {
      uuid: { type: 'string' }
    },
    interest: {
      displayImage: { type: 'string', required: true },
      name: { type: 'string', required: true },
      tag: { type: 'string', required: true }
    },
    language: {
      code: { type: 'string', required: true },
      name: { type: 'string', required: true }
    }
  }
};