import _ from 'lodash';
import Jsona from 'jsona';
import PropTypes from 'prop-types';

import config from '../config';
import RequestWatcher from './request-watcher';
import schema from './schema';

const dataFormatter = new Jsona();

let _headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

export function headers() {
  return _headers;
}

export function parseJSON(response) {
  if (response.ok) {
    return response.json();
  }
  return Promise.reject(response);
}

export function updateHeaders(newHeaders) {
  _headers = { ..._headers, newHeaders };
  Object.keys(_headers).forEach((key) => {
    if (undefined === _headers[key]) {
      delete _headers[key];
    }
  });
}

export const requestWatcher = new RequestWatcher();

const propTypeMap = {
  'string': {
    propType: PropTypes.string,
    default: ''
  },
  'object': {
    propType: PropTypes.object,
    default: {}
  }
};

const relationshipTypes = ['hasOne', 'hasMany'];

export function getAttributes(model) {
  return Object.entries(schema.models[model])
    .filter(v => !relationshipTypes.includes(v[1].type))
    .map(v => v[0]);
}

export function getRelationships(model) {
  return Object.entries(schema.models[model])
    .filter(v => relationshipTypes.includes(v[1].type))
    .map(v => v[0]);
}

export function initializeRecord(model, data) {
  const relationshipNames = getRelationships(model);

  return {
    type: decamelized(model),
    ...data,
    relationshipNames
  };
}

export function pushToKeyedMap(object, newObject) {
  let values = Object.values(object);
  values.push(newObject);

  return values.reduce((accumulator, value, index) => {
      accumulator[index.toString()] = value;
      return accumulator;
    }, {});
}

export function createDefaultProps(model) {
  let attributes = {};

  _.forOwn(schema.models[model], (value, key) => {
    if (relationshipTypes.includes(value.type)) {
      const defaultProps = createDefaultProps(value.model);
      attributes[key] = (value.type === 'hasMany') ? {} : defaultProps;
    } else {
      attributes[key] = propTypeMap[value.type].default;
    }
  });

  const defaultProps = {
    type: model,
    ...attributes
  };

  return defaultProps;
}

export function createPropTypes(model) {
  let attributes = {};

  _.forOwn(schema.models[model], (value, key) => {
    if (relationshipTypes.includes(value.type)) {
      const propTypes = createPropTypes(value.model);
      attributes[key] = (value.type === 'hasMany') ? PropTypes.object : propTypes;
    } else {
      attributes[key] = (value.required) ?
        propTypeMap[value.type].propType.isRequired :
        propTypeMap[value.type].propType;
    }
  });

  const propTypes = PropTypes.shape({
    type: PropTypes.string.isRequired,
    id: PropTypes.string,
    ...attributes
  });

  return propTypes;
}

export function camelized(names) {
  return _.camelCase(names);
}

export function decamelized(names) {
  return _.kebabCase(names);
}

const excludeFields = ['relationshipNames'];

function _normalized(object) {
  let newObject = {};

  _.forOwn(object, (value, key) => {
    if (!excludeFields.includes(key)) {
      const camelizedKey = camelized(key);
      newObject[camelizedKey] = (_.isObjectLike(value)) ? _normalized(value) : value;
    } else {
      newObject[key.toString()] = value;
    }
  });

  return newObject;
}

export function normalized(data) {
  return _normalized(dataFormatter.deserialize(data));
}

function _denormalized(object) {
  let newObject = {};

  // empty object is not serializable, so return an empty array instead
  if (_.isPlainObject(object) && _.isEmpty(object)) {
    return [];
  }

  _.forOwn(object, (value, key) => {
    if (!excludeFields.includes(key)) {
      const decamelizedKey = decamelized(key);
      newObject[decamelizedKey] = (_.isObjectLike(value)) ? _denormalized(value) : value;
    } else {
      newObject[key.toString()] = value;
    }
  });

  // quick check if newObject is serializable into an array
  if (newObject.hasOwnProperty('0')) {
    return Object.values(newObject);
  }

  return newObject;
}

export function denormalized(data) {
  data = _denormalized(data);

  return dataFormatter.serialize({
    stuff: data,
    includeNames: decamelized(data.relationshipNames)
  });
}

export function getProfilePhotoURL(profile, photo) {
  return `${config.baseURL}${config.uploadsPath}/base/profiles/${profile.id}/${photo.uuid}-thumb.png`;
}
