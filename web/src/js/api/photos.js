import { api } from '../api';

export async function getPhotos(profileId, include = []) {
  const response = await api.get(`/profiles/${profileId}/photos`);

  return { response, photos: response.data };
}
