import _ from 'lodash';

import { api, storage } from '../api';
import { initializeRecord } from './utils';

export async function getProfiles(include = []) {
  const response = await api.get('/profiles', {
    params: { include }
  });

  return { response, profiles: response.data };
}

export async function getProfile(id, include = []) {
  const response = await api.get(`/profiles/${id}`, {
    params: { include }
  });

  return { response, profile: response.data };
}

export async function saveProfile(data, include = []) {
  try {
    let { profile, profileMeta, photos } = data;

    let response;

    if (profile.id) {
      response = await api.put(`/profiles/${profile.id}`, profile);
    } else {
      response = await api.post('/profiles',
                                initializeRecord('profile', profile));
    }

    profile = response.data;

    await api.map(profileMeta, meta => {
      if (meta.id) {
        return api.put(`/profiles/${profile.id}/profile-meta/${meta.id}`, meta);
      }

      return api.post(`/profiles/${profile.id}/profile-meta/`,
                      initializeRecord('profileMeta', meta));
    });

    await api.map(photos, photo => {
      if (photo instanceof File) {
        let data = new FormData();
        data.append('data[profile_id]', profile.id);
        data.append('data[attachment]', photo);

        return storage.post('/photos/', data);
      }

      if (photo.deleted) {
        return api.delete(`/profiles/${profile.id}/photos/${photo.id}`);
      }
    });

    return getProfile(profile.id, include);
  } catch(error) {
    throw error;
  }
}
