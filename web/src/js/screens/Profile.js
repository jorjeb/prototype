import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';
import Notification from 'grommet/components/Notification';
import LinkPrevious from 'grommet/components/icons/base/LinkPrevious';
import Toast from 'grommet/components/Toast';

import {
  loadProfile, unloadProfile,
  addProfile, saveProfile, finishProfileEdit } from '../actions/profiles';
import { loadLanguages, unloadLanguages } from '../actions/languages';
import { loadInterests, unloadInterests } from '../actions/interests';
import { loadCountries, unloadCountries } from '../actions/countries';

import ProfileForm from '../components/ProfileForm';

import { pageLoaded, getFormErrors } from './utils';

class Profile extends Component {
  componentDidMount() {
    const { match: { params }, dispatch } = this.props;

    pageLoaded('Profile');

    if (params.id === 'add') {
      dispatch(addProfile());
    } else {
      dispatch(loadProfile(params.id));
    }

    dispatch(loadLanguages());
    dispatch(loadInterests());
    dispatch(loadCountries());

    this._onSubmit = this._onSubmit.bind(this);
    this._onClose = this._onClose.bind(this);
  }

  componentWillUnmount() {
    const { match: { params }, dispatch } = this.props;

    dispatch(unloadProfile());
    dispatch(unloadLanguages());
    dispatch(unloadInterests());
    dispatch(unloadCountries());
  }

  _onSubmit(data) {
    const { dispatch } = this.props;
    const { profile, profileMeta, photos } = data;

    dispatch(saveProfile({ profile, profileMeta, photos }));
  }

  _onClose() {
    const { dispatch } = this.props;

    dispatch(finishProfileEdit());
  }

  render() {
    const {
      profile,
      languages, interests, countries,
      error, saved, redirect, pathname
    } = this.props;

    let notificationNode;
    let redirectNode;
    let profileNode;

    let formErrors = {};

    if (error) {
      if (error.response && error.response.data.errors) {
        notificationNode = (
          <Toast status="ok">
            Please correct the fields highlighted in red
          </Toast>
        );

        formErrors = getFormErrors(error.response.data.errors);
      } else {
        notificationNode = (
          <Notification
            status="critical"
            size="small"
            state={error.message}
            message="An unexpected error happened, please try again later"
          />
        );
      }
    } else if(saved) {
      notificationNode = (
        <Toast status="ok" onClose={this._onClose}>
          Profile saved!
        </Toast>
      );
    }

    if (redirect) {
      redirectNode = (
        <Redirect push to={{
          pathname: pathname,
          state: { saved }
        }} />
      );
    }

    profileNode = (
      <Box pad="medium">
        <ProfileForm
          profile={profile}
          languages={languages}
          interests={interests}
          countries={countries}
          formErrors={formErrors}
          onSubmit={this._onSubmit}
        />
      </Box>
    );

    return (
      <Article primary={true} full={true}>
        <Header
          direction="row"
          size="large"
          colorIndex="light-2"
          align="center"
          responsive={false}
          pad={{ horizontal: 'small' }}
        >
          <Anchor path="/profiles">
            <LinkPrevious a11yTitle="Back to Profiles" />
          </Anchor>
          <Heading margin="none" strong={true}>
            {((profile && profile.id) ? 'Edit ' : 'Add ') + 'Profile'}
          </Heading>
        </Header>
        {notificationNode}
        {redirectNode}

        {profileNode}
      </Article>
    );
  }
}

Profile.defaultProps = {
  error: undefined,
  profile: undefined,
  countries: {},
  interests: {},
  languages: {},
  saved: false,
  redirect: false,
  pathname: ''
};

Profile.propTypes = {
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.object,
  match: PropTypes.object.isRequired,
  profile: PropTypes.object,
  languages: PropTypes.object,
  interests: PropTypes.object,
  countries: PropTypes.object,
  saved: PropTypes.bool,
  redirect: PropTypes.bool,
  pathname: PropTypes.string
};

const select = state => ({ ...state.profiles });

export default connect(select)(Profile);
