import { announcePageLoaded } from 'grommet/utils/Announcer';

import { camelized } from '../api/utils';

const DEFAULT_TITLE = 'Web';

export function pageLoaded(title) {
  if (document) {
    if (title && typeof title === 'string') {
      title = `${title} | ${DEFAULT_TITLE}`;
    } else {
      title = DEFAULT_TITLE;
    }
    announcePageLoaded(title);
    document.title = title;
  }
}

export function getFormErrors(errors) {
  return errors.reduce((accumulator, value) => {
    const path = value.source.pointer.split('/');
    const fieldName = camelized(path[path.length - 1]);

    accumulator[fieldName] = value.title;
    return accumulator;
  }, {});
}

export default { pageLoaded };
