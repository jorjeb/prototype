import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import AddIcon from 'grommet/components/icons/base/Add';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';
import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Meter from 'grommet/components/Meter';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import SearchInput from 'grommet/components/SearchInput';
import Spinning from 'grommet/components/icons/Spinning';
import { getMessage } from 'grommet/utils/Intl';

import NavControl from '../components/NavControl';

import _ from 'lodash';

import {
  loadProfiles, unloadProfiles
} from '../actions/profiles';

import { pageLoaded } from './utils';
import { createPropTypes } from '../api/utils';

class Profiles extends Component {
  componentDidMount() {
    pageLoaded('Profiles');

    this.props.dispatch(loadProfiles());
  }

  componentWillUnmount() {
    this.props.dispatch(unloadProfiles());
  }

  render() {
    const { error, profiles } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;
    if (error) {
      errorNode = (
        <Notification
          status="critical"
          size="large"
          state={error.message}
          message="An unexpected error happened, please try again later"
        />
      );
    } else {
      const profilesNode = _.map(profiles, profile => (
        <ListItem
          key={`profile_${profile.id}`}
          justify="between"
        >
          <Label><Anchor path={`/profiles/${profile.id}`} label={profile.displayName} /></Label>
        </ListItem>
      ));

      listNode = (
        <Box
          direction="column"
          pad={{ between: 'small', horizontal: 'medium', vertical: 'medium' }}
        >
          <Anchor icon={<AddIcon />}
            label="Add"
            path="/profiles/add"
            primary={true} />
          <List>
            {profilesNode}
          </List>
        </Box>
      );
    }

    return (
      <Article primary={true}>
        <Header
          direction="row"
          justify="between"
          size="large"
          pad={{ horizontal: 'medium', between: 'small' }}
        >
          <NavControl name={getMessage(intl, 'Profiles')} />
          <SearchInput placeHolder="Search" />
        </Header>
        {errorNode}
        {listNode}
      </Article>
    );
  }
}

Profiles.defaultProps = {
  error: undefined,
  profiles: {}
};

Profiles.propTypes = {
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.object,
  profiles: PropTypes.object
};

Profiles.contextTypes = {
  intl: PropTypes.object
};

const select = state => ({ ...state.profiles });

export default connect(select)(Profiles);
