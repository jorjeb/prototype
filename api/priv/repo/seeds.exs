# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Api.Repo.insert!(%Api.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Api.Repo
alias Api.Base.{Language, Country}

Repo.delete_all Language

Repo.insert! %Language{
    code: "da",
    name: "Danish"
}

Repo.insert! %Language{
    code: "nl",
    name: "Dutch"
}

Repo.insert! %Language{
    code: "en",
    name: "English"
}

Repo.insert! %Language{
    code: "fi",
    name: "Finnish"
}

Repo.insert! %Language{
    code: "fr",
    name: "French"
}

Repo.insert! %Language{
    code: "de",
    name: "German"
}

Repo.insert! %Language{
    code: "it",
    name: "Italian"
}

Repo.insert! %Language{
    code: "no",
    name: "Norwegian"
}

Repo.insert! %Language{
    code: "pt",
    name: "Portuguese"
}

Repo.insert! %Language{
    code: "es",
    name: "Spanish"
}

Repo.insert! %Language{
    code: "sv",
    name: "Swedish"
}

Repo.insert! %Language{
    code: "ru",
    name: "Russian"
}

Repo.delete_all Country

Repo.insert! %Country{
    code: "AF",
    name: "Afghanistan"
}

Repo.insert! %Country{
    code: "AL",
    name: "Albania"
}

Repo.insert! %Country{
    code: "DZ",
    name: "Algeria"
}

Repo.insert! %Country{
    code: "AS",
    name: "American Samoa"
}

Repo.insert! %Country{
    code: "AD",
    name: "Andorra"
}

Repo.insert! %Country{
    code: "AO",
    name: "Angola"
}

Repo.insert! %Country{
    code: "AI",
    name: "Anguilla"
}

Repo.insert! %Country{
    code: "AQ",
    name: "Antarctica"
}

Repo.insert! %Country{
    code: "AG",
    name: "Antigua and Barbuda"
}

Repo.insert! %Country{
    code: "AR",
    name: "Argentina"
}

Repo.insert! %Country{
    code: "AM",
    name: "Armenia"
}

Repo.insert! %Country{
    code: "AW",
    name: "Aruba"
}

Repo.insert! %Country{
    code: "AU",
    name: "Australia"
}

Repo.insert! %Country{
    code: "AT",
    name: "Austria"
}

Repo.insert! %Country{
    code: "AZ",
    name: "Azerbaijan"
}

Repo.insert! %Country{
    code: "BS",
    name: "Bahamas"
}

Repo.insert! %Country{
    code: "BH",
    name: "Bahrain"
}

Repo.insert! %Country{
    code: "BD",
    name: "Bangladesh"
}

Repo.insert! %Country{
    code: "BB",
    name: "Barbados"
}

Repo.insert! %Country{
    code: "BY",
    name: "Belarus"
}

Repo.insert! %Country{
    code: "BE",
    name: "Belgium"
}

Repo.insert! %Country{
    code: "BZ",
    name: "Belize"
}

Repo.insert! %Country{
    code: "BJ",
    name: "Benin"
}

Repo.insert! %Country{
    code: "BM",
    name: "Bermuda"
}

Repo.insert! %Country{
    code: "BT",
    name: "Bhutan"
}

Repo.insert! %Country{
    code: "BO",
    name: "Bolivia"
}

Repo.insert! %Country{
    code: "BA",
    name: "Bosnia and Herzegovina"
}

Repo.insert! %Country{
    code: "BW",
    name: "Botswana"
}

Repo.insert! %Country{
    code: "BV",
    name: "Bouvet Island"
}

Repo.insert! %Country{
    code: "BR",
    name: "Brazil"
}

Repo.insert! %Country{
    code: "BQ",
    name: "British Antarctic Territory"
}

Repo.insert! %Country{
    code: "IO",
    name: "British Indian Ocean Territory"
}

Repo.insert! %Country{
    code: "VG",
    name: "British Virgin Islands"
}

Repo.insert! %Country{
    code: "BN",
    name: "Brunei"
}

Repo.insert! %Country{
    code: "BG",
    name: "Bulgaria"
}

Repo.insert! %Country{
    code: "BF",
    name: "Burkina Faso"
}

Repo.insert! %Country{
    code: "BI",
    name: "Burundi"
}

Repo.insert! %Country{
    code: "KH",
    name: "Cambodia"
}

Repo.insert! %Country{
    code: "CM",
    name: "Cameroon"
}

Repo.insert! %Country{
    code: "CA",
    name: "Canada"
}

Repo.insert! %Country{
    code: "CT",
    name: "Canton and Enderbury Islands"
}

Repo.insert! %Country{
    code: "CV",
    name: "Cape Verde"
}

Repo.insert! %Country{
    code: "KY",
    name: "Cayman Islands"
}

Repo.insert! %Country{
    code: "CF",
    name: "Central African Republic"
}

Repo.insert! %Country{
    code: "TD",
    name: "Chad"
}

Repo.insert! %Country{
    code: "CL",
    name: "Chile"
}

Repo.insert! %Country{
    code: "CN",
    name: "China"
}

Repo.insert! %Country{
    code: "CX",
    name: "Christmas Island"
}

Repo.insert! %Country{
    code: "CC",
    name: "Cocos [Keeling] Islands"
}

Repo.insert! %Country{
    code: "CO",
    name: "Colombia"
}

Repo.insert! %Country{
    code: "KM",
    name: "Comoros"
}

Repo.insert! %Country{
    code: "CG",
    name: "Congo - Brazzaville"
}

Repo.insert! %Country{
    code: "CD",
    name: "Congo - Kinshasa"
}

Repo.insert! %Country{
    code: "CK",
    name: "Cook Islands"
}

Repo.insert! %Country{
    code: "CR",
    name: "Costa Rica"
}

Repo.insert! %Country{
    code: "HR",
    name: "Croatia"
}

Repo.insert! %Country{
    code: "CU",
    name: "Cuba"
}

Repo.insert! %Country{
    code: "CY",
    name: "Cyprus"
}

Repo.insert! %Country{
    code: "CZ",
    name: "Czech Republic"
}

Repo.insert! %Country{
    code: "CI",
    name: "Côte d’Ivoire"
}

Repo.insert! %Country{
    code: "DK",
    name: "Denmark"
}

Repo.insert! %Country{
    code: "DJ",
    name: "Djibouti"
}

Repo.insert! %Country{
    code: "DM",
    name: "Dominica"
}

Repo.insert! %Country{
    code: "DO",
    name: "Dominican Republic"
}

Repo.insert! %Country{
    code: "NQ",
    name: "Dronning Maud Land"
}

Repo.insert! %Country{
    code: "DD",
    name: "East Germany"
}

Repo.insert! %Country{
    code: "EC",
    name: "Ecuador"
}

Repo.insert! %Country{
    code: "EG",
    name: "Egypt"
}

Repo.insert! %Country{
    code: "SV",
    name: "El Salvador"
}

Repo.insert! %Country{
    code: "GQ",
    name: "Equatorial Guinea"
}

Repo.insert! %Country{
    code: "ER",
    name: "Eritrea"
}

Repo.insert! %Country{
    code: "EE",
    name: "Estonia"
}

Repo.insert! %Country{
    code: "ET",
    name: "Ethiopia"
}

Repo.insert! %Country{
    code: "FK",
    name: "Falkland Islands"
}

Repo.insert! %Country{
    code: "FO",
    name: "Faroe Islands"
}

Repo.insert! %Country{
    code: "FJ",
    name: "Fiji"
}

Repo.insert! %Country{
    code: "FI",
    name: "Finland"
}

Repo.insert! %Country{
    code: "FR",
    name: "France"
}

Repo.insert! %Country{
    code: "GF",
    name: "French Guiana"
}

Repo.insert! %Country{
    code: "PF",
    name: "French Polynesia"
}

Repo.insert! %Country{
    code: "TF",
    name: "French Southern Territories"
}

Repo.insert! %Country{
    code: "FQ",
    name: "French Southern and Antarctic Territories"
}

Repo.insert! %Country{
    code: "GA",
    name: "Gabon"
}

Repo.insert! %Country{
    code: "GM",
    name: "Gambia"
}

Repo.insert! %Country{
    code: "GE",
    name: "Georgia"
}

Repo.insert! %Country{
    code: "DE",
    name: "Germany"
}

Repo.insert! %Country{
    code: "GH",
    name: "Ghana"
}

Repo.insert! %Country{
    code: "GI",
    name: "Gibraltar"
}

Repo.insert! %Country{
    code: "GR",
    name: "Greece"
}

Repo.insert! %Country{
    code: "GL",
    name: "Greenland"
}

Repo.insert! %Country{
    code: "GD",
    name: "Grenada"
}

Repo.insert! %Country{
    code: "GP",
    name: "Guadeloupe"
}

Repo.insert! %Country{
    code: "GU",
    name: "Guam"
}

Repo.insert! %Country{
    code: "GT",
    name: "Guatemala"
}

Repo.insert! %Country{
    code: "GG",
    name: "Guernsey"
}

Repo.insert! %Country{
    code: "GN",
    name: "Guinea"
}

Repo.insert! %Country{
    code: "GW",
    name: "Guinea-Bissau"
}

Repo.insert! %Country{
    code: "GY",
    name: "Guyana"
}

Repo.insert! %Country{
    code: "HT",
    name: "Haiti"
}

Repo.insert! %Country{
    code: "HM",
    name: "Heard Island and McDonald Islands"
}

Repo.insert! %Country{
    code: "HN",
    name: "Honduras"
}

Repo.insert! %Country{
    code: "HK",
    name: "Hong Kong SAR China"
}

Repo.insert! %Country{
    code: "HU",
    name: "Hungary"
}

Repo.insert! %Country{
    code: "IS",
    name: "Iceland"
}

Repo.insert! %Country{
    code: "IN",
    name: "India"
}

Repo.insert! %Country{
    code: "ID",
    name: "Indonesia"
}

Repo.insert! %Country{
    code: "IR",
    name: "Iran"
}

Repo.insert! %Country{
    code: "IQ",
    name: "Iraq"
}

Repo.insert! %Country{
    code: "IE",
    name: "Ireland"
}

Repo.insert! %Country{
    code: "IM",
    name: "Isle of Man"
}

Repo.insert! %Country{
    code: "IL",
    name: "Israel"
}

Repo.insert! %Country{
    code: "IT",
    name: "Italy"
}

Repo.insert! %Country{
    code: "JM",
    name: "Jamaica"
}

Repo.insert! %Country{
    code: "JP",
    name: "Japan"
}

Repo.insert! %Country{
    code: "JE",
    name: "Jersey"
}

Repo.insert! %Country{
    code: "JT",
    name: "Johnston Island"
}

Repo.insert! %Country{
    code: "JO",
    name: "Jordan"
}

Repo.insert! %Country{
    code: "KZ",
    name: "Kazakhstan"
}

Repo.insert! %Country{
    code: "KE",
    name: "Kenya"
}

Repo.insert! %Country{
    code: "KI",
    name: "Kiribati"
}

Repo.insert! %Country{
    code: "KW",
    name: "Kuwait"
}

Repo.insert! %Country{
    code: "KG",
    name: "Kyrgyzstan"
}

Repo.insert! %Country{
    code: "LA",
    name: "Laos"
}

Repo.insert! %Country{
    code: "LV",
    name: "Latvia"
}

Repo.insert! %Country{
    code: "LB",
    name: "Lebanon"
}

Repo.insert! %Country{
    code: "LS",
    name: "Lesotho"
}

Repo.insert! %Country{
    code: "LR",
    name: "Liberia"
}

Repo.insert! %Country{
    code: "LY",
    name: "Libya"
}

Repo.insert! %Country{
    code: "LI",
    name: "Liechtenstein"
}

Repo.insert! %Country{
    code: "LT",
    name: "Lithuania"
}

Repo.insert! %Country{
    code: "LU",
    name: "Luxembourg"
}

Repo.insert! %Country{
    code: "MO",
    name: "Macau SAR China"
}

Repo.insert! %Country{
    code: "MK",
    name: "Macedonia"
}

Repo.insert! %Country{
    code: "MG",
    name: "Madagascar"
}

Repo.insert! %Country{
    code: "MW",
    name: "Malawi"
}

Repo.insert! %Country{
    code: "MY",
    name: "Malaysia"
}

Repo.insert! %Country{
    code: "MV",
    name: "Maldives"
}

Repo.insert! %Country{
    code: "ML",
    name: "Mali"
}

Repo.insert! %Country{
    code: "MT",
    name: "Malta"
}

Repo.insert! %Country{
    code: "MH",
    name: "Marshall Islands"
}

Repo.insert! %Country{
    code: "MQ",
    name: "Martinique"
}

Repo.insert! %Country{
    code: "MR",
    name: "Mauritania"
}

Repo.insert! %Country{
    code: "MU",
    name: "Mauritius"
}

Repo.insert! %Country{
    code: "YT",
    name: "Mayotte"
}

Repo.insert! %Country{
    code: "FX",
    name: "Metropolitan France"
}

Repo.insert! %Country{
    code: "MX",
    name: "Mexico"
}

Repo.insert! %Country{
    code: "FM",
    name: "Micronesia"
}

Repo.insert! %Country{
    code: "MI",
    name: "Midway Islands"
}

Repo.insert! %Country{
    code: "MD",
    name: "Moldova"
}

Repo.insert! %Country{
    code: "MC",
    name: "Monaco"
}

Repo.insert! %Country{
    code: "MN",
    name: "Mongolia"
}

Repo.insert! %Country{
    code: "ME",
    name: "Montenegro"
}

Repo.insert! %Country{
    code: "MS",
    name: "Montserrat"
}

Repo.insert! %Country{
    code: "MA",
    name: "Morocco"
}

Repo.insert! %Country{
    code: "MZ",
    name: "Mozambique"
}

Repo.insert! %Country{
    code: "MM",
    name: "Myanmar [Burma]"
}

Repo.insert! %Country{
    code: "NA",
    name: "Namibia"
}

Repo.insert! %Country{
    code: "NR",
    name: "Nauru"
}

Repo.insert! %Country{
    code: "NP",
    name: "Nepal"
}

Repo.insert! %Country{
    code: "NL",
    name: "Netherlands"
}

Repo.insert! %Country{
    code: "AN",
    name: "Netherlands Antilles"
}

Repo.insert! %Country{
    code: "NT",
    name: "Neutral Zone"
}

Repo.insert! %Country{
    code: "NC",
    name: "New Caledonia"
}

Repo.insert! %Country{
    code: "NZ",
    name: "New Zealand"
}

Repo.insert! %Country{
    code: "NI",
    name: "Nicaragua"
}

Repo.insert! %Country{
    code: "NE",
    name: "Niger"
}

Repo.insert! %Country{
    code: "NG",
    name: "Nigeria"
}

Repo.insert! %Country{
    code: "NU",
    name: "Niue"
}

Repo.insert! %Country{
    code: "NF",
    name: "Norfolk Island"
}

Repo.insert! %Country{
    code: "KP",
    name: "North Korea"
}

Repo.insert! %Country{
    code: "VD",
    name: "North Vietnam"
}

Repo.insert! %Country{
    code: "MP",
    name: "Northern Mariana Islands"
}

Repo.insert! %Country{
    code: "NO",
    name: "Norway"
}

Repo.insert! %Country{
    code: "OM",
    name: "Oman"
}

Repo.insert! %Country{
    code: "PC",
    name: "Pacific Islands Trust Territory"
}

Repo.insert! %Country{
    code: "PK",
    name: "Pakistan"
}

Repo.insert! %Country{
    code: "PW",
    name: "Palau"
}

Repo.insert! %Country{
    code: "PS",
    name: "Palestinian Territories"
}

Repo.insert! %Country{
    code: "PA",
    name: "Panama"
}

Repo.insert! %Country{
    code: "PZ",
    name: "Panama Canal Zone"
}

Repo.insert! %Country{
    code: "PG",
    name: "Papua New Guinea"
}

Repo.insert! %Country{
    code: "PY",
    name: "Paraguay"
}

Repo.insert! %Country{
    code: "YD",
    name: "People's Democratic Republic of Yemen"
}

Repo.insert! %Country{
    code: "PE",
    name: "Peru"
}

Repo.insert! %Country{
    code: "PH",
    name: "Philippines"
}

Repo.insert! %Country{
    code: "PN",
    name: "Pitcairn Islands"
}

Repo.insert! %Country{
    code: "PL",
    name: "Poland"
}

Repo.insert! %Country{
    code: "PT",
    name: "Portugal"
}

Repo.insert! %Country{
    code: "PR",
    name: "Puerto Rico"
}

Repo.insert! %Country{
    code: "QA",
    name: "Qatar"
}

Repo.insert! %Country{
    code: "RO",
    name: "Romania"
}

Repo.insert! %Country{
    code: "RU",
    name: "Russia"
}

Repo.insert! %Country{
    code: "RW",
    name: "Rwanda"
}

Repo.insert! %Country{
    code: "RE",
    name: "Réunion"
}

Repo.insert! %Country{
    code: "BL",
    name: "Saint Barthélemy"
}

Repo.insert! %Country{
    code: "SH",
    name: "Saint Helena"
}

Repo.insert! %Country{
    code: "KN",
    name: "Saint Kitts and Nevis"
}

Repo.insert! %Country{
    code: "LC",
    name: "Saint Lucia"
}

Repo.insert! %Country{
    code: "MF",
    name: "Saint Martin"
}

Repo.insert! %Country{
    code: "PM",
    name: "Saint Pierre and Miquelon"
}

Repo.insert! %Country{
    code: "VC",
    name: "Saint Vincent and the Grenadines"
}

Repo.insert! %Country{
    code: "WS",
    name: "Samoa"
}

Repo.insert! %Country{
    code: "SM",
    name: "San Marino"
}

Repo.insert! %Country{
    code: "SA",
    name: "Saudi Arabia"
}

Repo.insert! %Country{
    code: "SN",
    name: "Senegal"
}

Repo.insert! %Country{
    code: "RS",
    name: "Serbia"
}

Repo.insert! %Country{
    code: "CS",
    name: "Serbia and Montenegro"
}

Repo.insert! %Country{
    code: "SC",
    name: "Seychelles"
}

Repo.insert! %Country{
    code: "SL",
    name: "Sierra Leone"
}

Repo.insert! %Country{
    code: "SG",
    name: "Singapore"
}

Repo.insert! %Country{
    code: "SK",
    name: "Slovakia"
}

Repo.insert! %Country{
    code: "SI",
    name: "Slovenia"
}

Repo.insert! %Country{
    code: "SB",
    name: "Solomon Islands"
}

Repo.insert! %Country{
    code: "SO",
    name: "Somalia"
}

Repo.insert! %Country{
    code: "ZA",
    name: "South Africa"
}

Repo.insert! %Country{
    code: "GS",
    name: "South Georgia and the South Sandwich Islands"
}

Repo.insert! %Country{
    code: "KR",
    name: "South Korea"
}

Repo.insert! %Country{
    code: "ES",
    name: "Spain"
}

Repo.insert! %Country{
    code: "LK",
    name: "Sri Lanka"
}

Repo.insert! %Country{
    code: "SD",
    name: "Sudan"
}

Repo.insert! %Country{
    code: "SR",
    name: "Suriname"
}

Repo.insert! %Country{
    code: "SJ",
    name: "Svalbard and Jan Mayen"
}

Repo.insert! %Country{
    code: "SZ",
    name: "Swaziland"
}

Repo.insert! %Country{
    code: "SE",
    name: "Sweden"
}

Repo.insert! %Country{
    code: "CH",
    name: "Switzerland"
}

Repo.insert! %Country{
    code: "SY",
    name: "Syria"
}

Repo.insert! %Country{
    code: "ST",
    name: "São Tomé and Príncipe"
}

Repo.insert! %Country{
    code: "TW",
    name: "Taiwan"
}

Repo.insert! %Country{
    code: "TJ",
    name: "Tajikistan"
}

Repo.insert! %Country{
    code: "TZ",
    name: "Tanzania"
}

Repo.insert! %Country{
    code: "TH",
    name: "Thailand"
}

Repo.insert! %Country{
    code: "TL",
    name: "Timor-Leste"
}

Repo.insert! %Country{
    code: "TG",
    name: "Togo"
}

Repo.insert! %Country{
    code: "TK",
    name: "Tokelau"
}

Repo.insert! %Country{
    code: "TO",
    name: "Tonga"
}

Repo.insert! %Country{
    code: "TT",
    name: "Trinidad and Tobago"
}

Repo.insert! %Country{
    code: "TN",
    name: "Tunisia"
}

Repo.insert! %Country{
    code: "TR",
    name: "Turkey"
}

Repo.insert! %Country{
    code: "TM",
    name: "Turkmenistan"
}

Repo.insert! %Country{
    code: "TC",
    name: "Turks and Caicos Islands"
}

Repo.insert! %Country{
    code: "TV",
    name: "Tuvalu"
}

Repo.insert! %Country{
    code: "UM",
    name: "U.S. Minor Outlying Islands"
}

Repo.insert! %Country{
    code: "PU",
    name: "U.S. Miscellaneous Pacific Islands"
}

Repo.insert! %Country{
    code: "VI",
    name: "U.S. Virgin Islands"
}

Repo.insert! %Country{
    code: "UG",
    name: "Uganda"
}

Repo.insert! %Country{
    code: "UA",
    name: "Ukraine"
}

Repo.insert! %Country{
    code: "SU",
    name: "Union of Soviet Socialist Republics"
}

Repo.insert! %Country{
    code: "AE",
    name: "United Arab Emirates"
}

Repo.insert! %Country{
    code: "GB",
    name: "United Kingdom"
}

Repo.insert! %Country{
    code: "US",
    name: "United States"
}

Repo.insert! %Country{
    code: "ZZ",
    name: "Unknown or Invalid Region"
}

Repo.insert! %Country{
    code: "UY",
    name: "Uruguay"
}

Repo.insert! %Country{
    code: "UZ",
    name: "Uzbekistan"
}

Repo.insert! %Country{
    code: "VU",
    name: "Vanuatu"
}

Repo.insert! %Country{
    code: "VA",
    name: "Vatican City"
}

Repo.insert! %Country{
    code: "VE",
    name: "Venezuela"
}

Repo.insert! %Country{
    code: "VN",
    name: "Vietnam"
}

Repo.insert! %Country{
    code: "WK",
    name: "Wake Island"
}

Repo.insert! %Country{
    code: "WF",
    name: "Wallis and Futuna"
}

Repo.insert! %Country{
    code: "EH",
    name: "Western Sahara"
}

Repo.insert! %Country{
    code: "YE",
    name: "Yemen"
}

Repo.insert! %Country{
    code: "ZM",
    name: "Zambia"
}

Repo.insert! %Country{
    code: "ZW",
    name: "Zimbabwe"
}

Repo.insert! %Country{
    code: "AX",
    name: "Åland Islands"
}
