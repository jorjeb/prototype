defmodule Api.Repo.Migrations.RenamePhotosField do
  use Ecto.Migration

  def change do
    rename table(:photos), :file_name, to: :attachment
  end
end
