defmodule Api.Repo.Migrations.AddUniqueConstraintToInterestsTag do
  use Ecto.Migration

  def change do
    create unique_index(:interests, [:tag])
  end
end
