defmodule Api.Repo.Migrations.CreateProfileMeta do
  use Ecto.Migration

  def change do
    create table(:profile_meta) do
      add :blurb, :text
      add :living, :text
      add :smoking_habits, :text
      add :drinking_habits, :text
      add :profile_id, references(:profiles, on_delete: :delete_all)

      timestamps()
    end

    create index(:profile_meta, [:profile_id])
  end
end
