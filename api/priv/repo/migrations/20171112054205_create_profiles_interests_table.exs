defmodule Api.Repo.Migrations.CreateProfilesInterestsTable do
  use Ecto.Migration

  def change do
    create table(:profiles_interests, primary_key: false) do
      add :profile_id, references(:profiles, on_delete: :delete_all)
      add :interest_id, references(:interests, on_delete: :delete_all)
    end

    create index(:profiles_interests, [:profile_id])
    create index(:profiles_interests, [:interest_id])
  end
end
