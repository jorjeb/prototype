defmodule Api.Repo.Migrations.CreatePhotos do
  use Ecto.Migration

  def change do
    create table(:photos) do
      add :file_name, :text
      add :profile_id, references(:profiles, on_delete: :delete_all)

      timestamps()
    end

    create index(:photos, [:profile_id])
  end
end
