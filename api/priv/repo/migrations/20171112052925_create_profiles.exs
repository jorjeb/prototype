defmodule Api.Repo.Migrations.CreateProfiles do
  use Ecto.Migration

  def change do
    create table(:profiles) do
      add :display_name, :string
      add :height, :string
      add :weight, :string
      add :hair_colour, :string
      add :eye_colour, :string
      add :body_type, :string
      add :body_art, :string
      add :gender, :string
      add :looking_for, :string
      add :orientation, :string
      add :civil_status, :string
      add :ethnicity, :string
      add :star_sign, :string
      add :religion, :string
      add :education, :string
      add :children, :string

      timestamps()
    end

    create unique_index(:profiles, [:display_name])
  end
end
