defmodule Api.Repo.Migrations.AddLanguageIdToProfileMeta do
  use Ecto.Migration

  def change do
    alter table(:profile_meta) do
      add :language_id, references(:languages, on_delete: :delete_all)
    end

    create index(:profile_meta, [:language_id])
  end
end
