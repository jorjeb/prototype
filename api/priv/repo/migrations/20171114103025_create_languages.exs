defmodule Api.Repo.Migrations.CreateLanguages do
  use Ecto.Migration

  def change do
    create table(:languages) do
      add :code, :string, size: 2
      add :name, :string
    end

    create unique_index(:languages, [:code])
  end
end
