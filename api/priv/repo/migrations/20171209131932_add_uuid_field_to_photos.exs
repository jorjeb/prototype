defmodule Api.Repo.Migrations.AddUuidFieldToPhotos do
  use Ecto.Migration

  def change do
    alter table(:photos) do
      add :uuid, :uuid
    end
  end
end
