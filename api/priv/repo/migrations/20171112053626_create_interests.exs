defmodule Api.Repo.Migrations.CreateInterests do
  use Ecto.Migration

  def change do
    create table(:interests) do
      add :name, :string
      add :tag, :string
      add :display_image, :text

      timestamps()
    end

  end
end
