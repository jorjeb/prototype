defmodule Api.Repo.Migrations.AddCountryFieldToProfiles do
  use Ecto.Migration

  def change do
    alter table(:profiles) do
      add :country_id, references(:countries, on_delete: :delete_all)
    end

    create index(:profiles, [:country_id])
  end
end
