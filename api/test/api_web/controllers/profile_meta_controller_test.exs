defmodule ApiWeb.ProfileMetaControllerTest do
  use ApiWeb.ConnCase

  alias Api.Base
  alias Api.Base.ProfileMeta

  @create_attrs %{blurb: "some blurb", drinking_habits: "some drinking_habits", living: "some living", smoking_habits: "some smoking_habits"}
  @update_attrs %{blurb: "some updated blurb", drinking_habits: "some updated drinking_habits", living: "some updated living", smoking_habits: "some updated smoking_habits"}
  @invalid_attrs %{blurb: nil, drinking_habits: nil, living: nil, smoking_habits: nil}

  def fixture(:profile_meta) do
    {:ok, profile_meta} = Base.create_profile_meta(@create_attrs)
    profile_meta
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all profile_meta", %{conn: conn} do
      conn = get conn, profile_meta_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create profile_meta" do
    test "renders profile_meta when data is valid", %{conn: conn} do
      conn = post conn, profile_meta_path(conn, :create), profile_meta: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, profile_meta_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "blurb" => "some blurb",
        "drinking_habits" => "some drinking_habits",
        "living" => "some living",
        "smoking_habits" => "some smoking_habits"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, profile_meta_path(conn, :create), profile_meta: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update profile_meta" do
    setup [:create_profile_meta]

    test "renders profile_meta when data is valid", %{conn: conn, profile_meta: %ProfileMeta{id: id} = profile_meta} do
      conn = put conn, profile_meta_path(conn, :update, profile_meta), profile_meta: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, profile_meta_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "blurb" => "some updated blurb",
        "drinking_habits" => "some updated drinking_habits",
        "living" => "some updated living",
        "smoking_habits" => "some updated smoking_habits"}
    end

    test "renders errors when data is invalid", %{conn: conn, profile_meta: profile_meta} do
      conn = put conn, profile_meta_path(conn, :update, profile_meta), profile_meta: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete profile_meta" do
    setup [:create_profile_meta]

    test "deletes chosen profile_meta", %{conn: conn, profile_meta: profile_meta} do
      conn = delete conn, profile_meta_path(conn, :delete, profile_meta)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, profile_meta_path(conn, :show, profile_meta)
      end
    end
  end

  defp create_profile_meta(_) do
    profile_meta = fixture(:profile_meta)
    {:ok, profile_meta: profile_meta}
  end
end
