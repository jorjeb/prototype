defmodule ApiWeb.ProfileControllerTest do
  use ApiWeb.ConnCase

  alias Api.Base
  alias Api.Base.Profile

  @create_attrs %{body_art: "some body_art", body_type: "some body_type", children: "some children", civil_status: "some civil_status", display_name: "some display_name", education: "some education", ethnicity: "some ethnicity", eye_colour: "some eye_colour", gender: "some gender", hair_colour: "some hair_colour", height: "some height", looking_for: "some looking_for", orientation: "some orientation", religion: "some religion", star_sign: "some star_sign", weight: "some weight"}
  @update_attrs %{body_art: "some updated body_art", body_type: "some updated body_type", children: "some updated children", civil_status: "some updated civil_status", display_name: "some updated display_name", education: "some updated education", ethnicity: "some updated ethnicity", eye_colour: "some updated eye_colour", gender: "some updated gender", hair_colour: "some updated hair_colour", height: "some updated height", looking_for: "some updated looking_for", orientation: "some updated orientation", religion: "some updated religion", star_sign: "some updated star_sign", weight: "some updated weight"}
  @invalid_attrs %{body_art: nil, body_type: nil, children: nil, civil_status: nil, display_name: nil, education: nil, ethnicity: nil, eye_colour: nil, gender: nil, hair_colour: nil, height: nil, looking_for: nil, orientation: nil, religion: nil, star_sign: nil, weight: nil}

  def fixture(:profile) do
    {:ok, profile} = Base.create_profile(@create_attrs)
    profile
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all profiles", %{conn: conn} do
      conn = get conn, profile_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create profile" do
    test "renders profile when data is valid", %{conn: conn} do
      conn = post conn, profile_path(conn, :create), profile: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, profile_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "body_art" => "some body_art",
        "body_type" => "some body_type",
        "children" => "some children",
        "civil_status" => "some civil_status",
        "display_name" => "some display_name",
        "education" => "some education",
        "ethnicity" => "some ethnicity",
        "eye_colour" => "some eye_colour",
        "gender" => "some gender",
        "hair_colour" => "some hair_colour",
        "height" => "some height",
        "looking_for" => "some looking_for",
        "orientation" => "some orientation",
        "religion" => "some religion",
        "star_sign" => "some star_sign",
        "weight" => "some weight"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, profile_path(conn, :create), profile: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update profile" do
    setup [:create_profile]

    test "renders profile when data is valid", %{conn: conn, profile: %Profile{id: id} = profile} do
      conn = put conn, profile_path(conn, :update, profile), profile: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, profile_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "body_art" => "some updated body_art",
        "body_type" => "some updated body_type",
        "children" => "some updated children",
        "civil_status" => "some updated civil_status",
        "display_name" => "some updated display_name",
        "education" => "some updated education",
        "ethnicity" => "some updated ethnicity",
        "eye_colour" => "some updated eye_colour",
        "gender" => "some updated gender",
        "hair_colour" => "some updated hair_colour",
        "height" => "some updated height",
        "looking_for" => "some updated looking_for",
        "orientation" => "some updated orientation",
        "religion" => "some updated religion",
        "star_sign" => "some updated star_sign",
        "weight" => "some updated weight"}
    end

    test "renders errors when data is invalid", %{conn: conn, profile: profile} do
      conn = put conn, profile_path(conn, :update, profile), profile: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete profile" do
    setup [:create_profile]

    test "deletes chosen profile", %{conn: conn, profile: profile} do
      conn = delete conn, profile_path(conn, :delete, profile)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, profile_path(conn, :show, profile)
      end
    end
  end

  defp create_profile(_) do
    profile = fixture(:profile)
    {:ok, profile: profile}
  end
end
