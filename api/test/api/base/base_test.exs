defmodule Api.BaseTest do
  use Api.DataCase

  alias Api.Base

  describe "profiles" do
    alias Api.Base.Profile

    @valid_attrs %{body_art: "some body_art", body_type: "some body_type", children: "some children", civil_status: "some civil_status", display_name: "some display_name", education: "some education", ethnicity: "some ethnicity", eye_colour: "some eye_colour", gender: "some gender", hair_colour: "some hair_colour", height: "some height", looking_for: "some looking_for", orientation: "some orientation", religion: "some religion", star_sign: "some star_sign", weight: "some weight"}
    @update_attrs %{body_art: "some updated body_art", body_type: "some updated body_type", children: "some updated children", civil_status: "some updated civil_status", display_name: "some updated display_name", education: "some updated education", ethnicity: "some updated ethnicity", eye_colour: "some updated eye_colour", gender: "some updated gender", hair_colour: "some updated hair_colour", height: "some updated height", looking_for: "some updated looking_for", orientation: "some updated orientation", religion: "some updated religion", star_sign: "some updated star_sign", weight: "some updated weight"}
    @invalid_attrs %{body_art: nil, body_type: nil, children: nil, civil_status: nil, display_name: nil, education: nil, ethnicity: nil, eye_colour: nil, gender: nil, hair_colour: nil, height: nil, looking_for: nil, orientation: nil, religion: nil, star_sign: nil, weight: nil}

    def profile_fixture(attrs \\ %{}) do
      {:ok, profile} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Base.create_profile()

      profile
    end

    test "list_profiles/0 returns all profiles" do
      profile = profile_fixture()
      assert Base.list_profiles() == [profile]
    end

    test "get_profile!/1 returns the profile with given id" do
      profile = profile_fixture()
      assert Base.get_profile!(profile.id) == profile
    end

    test "create_profile/1 with valid data creates a profile" do
      assert {:ok, %Profile{} = profile} = Base.create_profile(@valid_attrs)
      assert profile.body_art == "some body_art"
      assert profile.body_type == "some body_type"
      assert profile.children == "some children"
      assert profile.civil_status == "some civil_status"
      assert profile.display_name == "some display_name"
      assert profile.education == "some education"
      assert profile.ethnicity == "some ethnicity"
      assert profile.eye_colour == "some eye_colour"
      assert profile.gender == "some gender"
      assert profile.hair_colour == "some hair_colour"
      assert profile.height == "some height"
      assert profile.looking_for == "some looking_for"
      assert profile.orientation == "some orientation"
      assert profile.religion == "some religion"
      assert profile.star_sign == "some star_sign"
      assert profile.weight == "some weight"
    end

    test "create_profile/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Base.create_profile(@invalid_attrs)
    end

    test "update_profile/2 with valid data updates the profile" do
      profile = profile_fixture()
      assert {:ok, profile} = Base.update_profile(profile, @update_attrs)
      assert %Profile{} = profile
      assert profile.body_art == "some updated body_art"
      assert profile.body_type == "some updated body_type"
      assert profile.children == "some updated children"
      assert profile.civil_status == "some updated civil_status"
      assert profile.display_name == "some updated display_name"
      assert profile.education == "some updated education"
      assert profile.ethnicity == "some updated ethnicity"
      assert profile.eye_colour == "some updated eye_colour"
      assert profile.gender == "some updated gender"
      assert profile.hair_colour == "some updated hair_colour"
      assert profile.height == "some updated height"
      assert profile.looking_for == "some updated looking_for"
      assert profile.orientation == "some updated orientation"
      assert profile.religion == "some updated religion"
      assert profile.star_sign == "some updated star_sign"
      assert profile.weight == "some updated weight"
    end

    test "update_profile/2 with invalid data returns error changeset" do
      profile = profile_fixture()
      assert {:error, %Ecto.Changeset{}} = Base.update_profile(profile, @invalid_attrs)
      assert profile == Base.get_profile!(profile.id)
    end

    test "delete_profile/1 deletes the profile" do
      profile = profile_fixture()
      assert {:ok, %Profile{}} = Base.delete_profile(profile)
      assert_raise Ecto.NoResultsError, fn -> Base.get_profile!(profile.id) end
    end

    test "change_profile/1 returns a profile changeset" do
      profile = profile_fixture()
      assert %Ecto.Changeset{} = Base.change_profile(profile)
    end
  end

  describe "interests" do
    alias Api.Base.Interest

    @valid_attrs %{display_image: "some display_image", name: "some name", tag: "some tag"}
    @update_attrs %{display_image: "some updated display_image", name: "some updated name", tag: "some updated tag"}
    @invalid_attrs %{display_image: nil, name: nil, tag: nil}

    def interest_fixture(attrs \\ %{}) do
      {:ok, interest} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Base.create_interest()

      interest
    end

    test "list_interests/0 returns all interests" do
      interest = interest_fixture()
      assert Base.list_interests() == [interest]
    end

    test "get_interest!/1 returns the interest with given id" do
      interest = interest_fixture()
      assert Base.get_interest!(interest.id) == interest
    end

    test "create_interest/1 with valid data creates a interest" do
      assert {:ok, %Interest{} = interest} = Base.create_interest(@valid_attrs)
      assert interest.display_image == "some display_image"
      assert interest.name == "some name"
      assert interest.tag == "some tag"
    end

    test "create_interest/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Base.create_interest(@invalid_attrs)
    end

    test "update_interest/2 with valid data updates the interest" do
      interest = interest_fixture()
      assert {:ok, interest} = Base.update_interest(interest, @update_attrs)
      assert %Interest{} = interest
      assert interest.display_image == "some updated display_image"
      assert interest.name == "some updated name"
      assert interest.tag == "some updated tag"
    end

    test "update_interest/2 with invalid data returns error changeset" do
      interest = interest_fixture()
      assert {:error, %Ecto.Changeset{}} = Base.update_interest(interest, @invalid_attrs)
      assert interest == Base.get_interest!(interest.id)
    end

    test "delete_interest/1 deletes the interest" do
      interest = interest_fixture()
      assert {:ok, %Interest{}} = Base.delete_interest(interest)
      assert_raise Ecto.NoResultsError, fn -> Base.get_interest!(interest.id) end
    end

    test "change_interest/1 returns a interest changeset" do
      interest = interest_fixture()
      assert %Ecto.Changeset{} = Base.change_interest(interest)
    end
  end

  describe "photos" do
    alias Api.Base.Photo

    @valid_attrs %{file_name: "some file_name"}
    @update_attrs %{file_name: "some updated file_name"}
    @invalid_attrs %{file_name: nil}

    def photo_fixture(attrs \\ %{}) do
      {:ok, photo} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Base.create_photo()

      photo
    end

    test "list_photos/0 returns all photos" do
      photo = photo_fixture()
      assert Base.list_photos() == [photo]
    end

    test "get_photo!/1 returns the photo with given id" do
      photo = photo_fixture()
      assert Base.get_photo!(photo.id) == photo
    end

    test "create_photo/1 with valid data creates a photo" do
      assert {:ok, %Photo{} = photo} = Base.create_photo(@valid_attrs)
      assert photo.file_name == "some file_name"
    end

    test "create_photo/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Base.create_photo(@invalid_attrs)
    end

    test "update_photo/2 with valid data updates the photo" do
      photo = photo_fixture()
      assert {:ok, photo} = Base.update_photo(photo, @update_attrs)
      assert %Photo{} = photo
      assert photo.file_name == "some updated file_name"
    end

    test "update_photo/2 with invalid data returns error changeset" do
      photo = photo_fixture()
      assert {:error, %Ecto.Changeset{}} = Base.update_photo(photo, @invalid_attrs)
      assert photo == Base.get_photo!(photo.id)
    end

    test "delete_photo/1 deletes the photo" do
      photo = photo_fixture()
      assert {:ok, %Photo{}} = Base.delete_photo(photo)
      assert_raise Ecto.NoResultsError, fn -> Base.get_photo!(photo.id) end
    end

    test "change_photo/1 returns a photo changeset" do
      photo = photo_fixture()
      assert %Ecto.Changeset{} = Base.change_photo(photo)
    end
  end

  describe "profile_meta" do
    alias Api.Base.ProfileMeta

    @valid_attrs %{blurb: "some blurb", drinking_habits: "some drinking_habits", living: "some living", smoking_habits: "some smoking_habits"}
    @update_attrs %{blurb: "some updated blurb", drinking_habits: "some updated drinking_habits", living: "some updated living", smoking_habits: "some updated smoking_habits"}
    @invalid_attrs %{blurb: nil, drinking_habits: nil, living: nil, smoking_habits: nil}

    def profile_meta_fixture(attrs \\ %{}) do
      {:ok, profile_meta} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Base.create_profile_meta()

      profile_meta
    end

    test "list_profile_meta/0 returns all profile_meta" do
      profile_meta = profile_meta_fixture()
      assert Base.list_profile_meta() == [profile_meta]
    end

    test "get_profile_meta!/1 returns the profile_meta with given id" do
      profile_meta = profile_meta_fixture()
      assert Base.get_profile_meta!(profile_meta.id) == profile_meta
    end

    test "create_profile_meta/1 with valid data creates a profile_meta" do
      assert {:ok, %ProfileMeta{} = profile_meta} = Base.create_profile_meta(@valid_attrs)
      assert profile_meta.blurb == "some blurb"
      assert profile_meta.drinking_habits == "some drinking_habits"
      assert profile_meta.living == "some living"
      assert profile_meta.smoking_habits == "some smoking_habits"
    end

    test "create_profile_meta/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Base.create_profile_meta(@invalid_attrs)
    end

    test "update_profile_meta/2 with valid data updates the profile_meta" do
      profile_meta = profile_meta_fixture()
      assert {:ok, profile_meta} = Base.update_profile_meta(profile_meta, @update_attrs)
      assert %ProfileMeta{} = profile_meta
      assert profile_meta.blurb == "some updated blurb"
      assert profile_meta.drinking_habits == "some updated drinking_habits"
      assert profile_meta.living == "some updated living"
      assert profile_meta.smoking_habits == "some updated smoking_habits"
    end

    test "update_profile_meta/2 with invalid data returns error changeset" do
      profile_meta = profile_meta_fixture()
      assert {:error, %Ecto.Changeset{}} = Base.update_profile_meta(profile_meta, @invalid_attrs)
      assert profile_meta == Base.get_profile_meta!(profile_meta.id)
    end

    test "delete_profile_meta/1 deletes the profile_meta" do
      profile_meta = profile_meta_fixture()
      assert {:ok, %ProfileMeta{}} = Base.delete_profile_meta(profile_meta)
      assert_raise Ecto.NoResultsError, fn -> Base.get_profile_meta!(profile_meta.id) end
    end

    test "change_profile_meta/1 returns a profile_meta changeset" do
      profile_meta = profile_meta_fixture()
      assert %Ecto.Changeset{} = Base.change_profile_meta(profile_meta)
    end
  end

  describe "languages" do
    alias Api.Base.Language

    @valid_attrs %{code: "some code", name: "some name"}
    @update_attrs %{code: "some updated code", name: "some updated name"}
    @invalid_attrs %{code: nil, name: nil}

    def language_fixture(attrs \\ %{}) do
      {:ok, language} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Base.create_language()

      language
    end

    test "list_languages/0 returns all languages" do
      language = language_fixture()
      assert Base.list_languages() == [language]
    end

    test "get_language!/1 returns the language with given id" do
      language = language_fixture()
      assert Base.get_language!(language.id) == language
    end

    test "create_language/1 with valid data creates a language" do
      assert {:ok, %Language{} = language} = Base.create_language(@valid_attrs)
      assert language.code == "some code"
      assert language.name == "some name"
    end

    test "create_language/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Base.create_language(@invalid_attrs)
    end

    test "update_language/2 with valid data updates the language" do
      language = language_fixture()
      assert {:ok, language} = Base.update_language(language, @update_attrs)
      assert %Language{} = language
      assert language.code == "some updated code"
      assert language.name == "some updated name"
    end

    test "update_language/2 with invalid data returns error changeset" do
      language = language_fixture()
      assert {:error, %Ecto.Changeset{}} = Base.update_language(language, @invalid_attrs)
      assert language == Base.get_language!(language.id)
    end

    test "delete_language/1 deletes the language" do
      language = language_fixture()
      assert {:ok, %Language{}} = Base.delete_language(language)
      assert_raise Ecto.NoResultsError, fn -> Base.get_language!(language.id) end
    end

    test "change_language/1 returns a language changeset" do
      language = language_fixture()
      assert %Ecto.Changeset{} = Base.change_language(language)
    end
  end

  describe "countries" do
    alias Api.Base.Country

    @valid_attrs %{code: "some code", name: "some name"}
    @update_attrs %{code: "some updated code", name: "some updated name"}
    @invalid_attrs %{code: nil, name: nil}

    def country_fixture(attrs \\ %{}) do
      {:ok, country} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Base.create_country()

      country
    end

    test "list_countries/0 returns all countries" do
      country = country_fixture()
      assert Base.list_countries() == [country]
    end

    test "get_country!/1 returns the country with given id" do
      country = country_fixture()
      assert Base.get_country!(country.id) == country
    end

    test "create_country/1 with valid data creates a country" do
      assert {:ok, %Country{} = country} = Base.create_country(@valid_attrs)
      assert country.code == "some code"
      assert country.name == "some name"
    end

    test "create_country/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Base.create_country(@invalid_attrs)
    end

    test "update_country/2 with valid data updates the country" do
      country = country_fixture()
      assert {:ok, country} = Base.update_country(country, @update_attrs)
      assert %Country{} = country
      assert country.code == "some updated code"
      assert country.name == "some updated name"
    end

    test "update_country/2 with invalid data returns error changeset" do
      country = country_fixture()
      assert {:error, %Ecto.Changeset{}} = Base.update_country(country, @invalid_attrs)
      assert country == Base.get_country!(country.id)
    end

    test "delete_country/1 deletes the country" do
      country = country_fixture()
      assert {:ok, %Country{}} = Base.delete_country(country)
      assert_raise Ecto.NoResultsError, fn -> Base.get_country!(country.id) end
    end

    test "change_country/1 returns a country changeset" do
      country = country_fixture()
      assert %Ecto.Changeset{} = Base.change_country(country)
    end
  end
end
