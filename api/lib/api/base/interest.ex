defmodule Api.Base.Interest do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Base.Interest


  schema "interests" do
    field :display_image, :string
    field :name, :string
    field :tag, :string

    timestamps()
  end

  @doc false
  def changeset(%Interest{} = interest, attrs) do
    interest
    |> cast(attrs, [:name, :tag, :display_image])
    |> validate_required([:name, :tag, :display_image])
    |> unique_constraint(:tag)
  end
end
