defmodule Api.Base.Language do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Base.Language


  schema "languages" do
    field :code, :string
    field :name, :string
  end

  @doc false
  def changeset(%Language{} = language, attrs) do
    language
    |> cast(attrs, [:code, :name])
    |> validate_required([:code, :name])
    |> unique_constraint(:code)
  end
end
