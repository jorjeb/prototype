defmodule Api.Base.Country do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Base.Country


  schema "countries" do
    field :code, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(%Country{} = country, attrs) do
    country
    |> cast(attrs, [:code, :name])
    |> validate_required([:code, :name])
  end
end
