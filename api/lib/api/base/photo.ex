defmodule Api.Base.Photo do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Api.Base.{Photo, Profile}
  alias Ecto.UUID


  schema "photos" do
    field :attachment, Api.Photo.Type
    field :uuid, UUID

    belongs_to :profile, Profile

    timestamps()
  end

  @doc false
  def changeset(%Photo{} = photo, attrs) do
    photo
    |> cast(attrs, [:profile_id])
    |> put_change(:uuid, UUID.generate())
    |> cast_attachments(attrs, [:attachment])
    |> validate_required([:attachment, :profile_id, :uuid])
  end

  def filter(query, params) do
    Enum.reduce(params, query, fn({key, value}, query) ->
      case String.downcase(key) do
        _ ->
          from p in query,
          where: field(p, ^String.to_atom(key)) == ^value
      end
    end)
  end
end
