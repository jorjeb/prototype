defmodule Api.Base.ProfileMeta do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Api.Base.{Profile, ProfileMeta, Language}


  schema "profile_meta" do
    field :blurb, :string
    field :drinking_habits, :string
    field :living, :string
    field :smoking_habits, :string

    belongs_to :profile, Profile
    belongs_to :language, Language

    timestamps()
  end

  @doc false
  def changeset(%ProfileMeta{} = profile_meta, attrs) do
    profile_meta
    |> cast(attrs, [:blurb, :drinking_habits, :smoking_habits, :living, :profile_id, :language_id])
    |> validate_required([:profile_id, :language_id])
  end

  def filter(query, params) do
    Enum.reduce(params, query, fn({key, value}, query) ->
      case String.downcase(key) do
        _ ->
          from p in query,
          where: field(p, ^String.to_atom(key)) == ^value
      end
    end)
  end
end
