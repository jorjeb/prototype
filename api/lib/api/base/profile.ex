defmodule Api.Base.Profile do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Api.Base.{Profile, Country, ProfileMeta, Photo, Interest}
  alias Api.Repo


  schema "profiles" do
    field :body_art, :string
    field :body_type, :string
    field :children, :string
    field :civil_status, :string
    field :display_name, :string
    field :education, :string
    field :ethnicity, :string
    field :eye_colour, :string
    field :gender, :string
    field :hair_colour, :string
    field :height, :string
    field :looking_for, :string
    field :orientation, :string
    field :religion, :string
    field :star_sign, :string
    field :weight, :string

    belongs_to :country, Country

    has_many :profile_meta, ProfileMeta
    has_many :photos, Photo
    many_to_many :interests, Interest, join_through: "profiles_interests", on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(%Profile{} = profile, attrs) do
    profile
    |> cast(attrs, [:display_name, :height, :weight, :hair_colour, :eye_colour, :body_type, :body_art, :gender, :looking_for, :orientation, :civil_status, :ethnicity, :star_sign, :religion, :education, :children, :country_id])
    |> validate_required([:display_name, :country_id])
    |> unique_constraint(:display_name)
    |> put_assoc(:interests, load_interests(attrs))
  end

  defp load_interests(attrs) do
    case attrs["interests_ids"] || [] do
      [] -> []
      ids -> Repo.all from i in Interest, where: i.id in ^ids
    end
  end
end
