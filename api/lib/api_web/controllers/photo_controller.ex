defmodule ApiWeb.PhotoController do
  use ApiWeb, :controller

  import ApiWeb.HttpPlug

  alias Api.Repo
  alias Api.Base
  alias Api.Base.Photo

  action_fallback ApiWeb.FallbackController

  plug :valid_filters, ~w(profile_id) when action in [:index]

  defp preload(photos) do
    photos
    |> Repo.preload([:profile])
  end

  def index(conn, _params) do
    photos = Photo
      |> Photo.filter(conn.assigns.filters)
      |> Repo.all

    render(conn, "index.json-api", data: preload(photos))
  end

  def create(conn, %{"data" => data}) do
    case Base.create_photo(data) do
      {:ok, %Photo{} = photo} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location",
                           profile_photo_path(conn,
                                              :show,
                                              preload(photo).profile,
                                              photo))
        |> render("show.json-api", data: preload(photo))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(:errors, data: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    photo = Base.get_photo!(id)
    render(conn, "show.json-api", data: preload(photo))
  end

  def delete(conn, %{"id" => id}) do
    photo = Base.get_photo!(id)
    with {:ok, %Photo{}} <- Base.delete_photo(photo) do
      send_resp(conn, :no_content, "")
    end
  end
end
