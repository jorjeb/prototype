defmodule ApiWeb.ProfileController do
  use ApiWeb, :controller

  alias Api.Base
  alias Api.Base.Profile
  alias Api.Repo

  action_fallback ApiWeb.FallbackController

  defp preload(profiles) do
    profiles
    |> Repo.preload([profile_meta: [:language], photos: [], interests: [], country: []])
  end

  def index(conn, _params) do
    profiles = Base.list_profiles()
    render(conn, "index.json-api",
           data: preload(profiles),
           opts: [include: conn.params["include"]])
  end

  def create(conn, %{"data" => data}) do
    attrs = JaSerializer.Params.to_attributes(data)

    case Base.create_profile(attrs) do
      {:ok, %Profile{} = profile} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", profile_path(conn, :show, profile))
        |> render("show.json-api", data: preload(profile))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(:errors, data: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    profile = Base.get_profile!(id)
    render(conn, "show.json-api",
           data: preload(profile),
           opts: [include: conn.params["include"]])
  end

  def update(conn, %{"data" => %{"id" => id}} = data) do
    profile = Base.get_profile!(id)
      |> Repo.preload(:interests)
    attrs = JaSerializer.Params.to_attributes(data)

    case Base.update_profile(profile, attrs) do
      {:ok, %Profile{} = profile} ->
        render(conn, "show.json-api", data: preload(profile))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(:errors, data: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    profile = Base.get_profile!(id)

    with {:ok, %Profile{}} <- Base.delete_profile(profile) do
      send_resp(conn, :no_content, "")
    end
  end
end
