defmodule ApiWeb.InterestController do
  use ApiWeb, :controller

  alias Api.Base
  alias Api.Base.Interest

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    interests = Base.list_interests()
    render(conn, "index.json-api", data: interests)
  end

  def create(conn, %{"data" => data}) do
    attrs = JaSerializer.Params.to_attributes(data)

    with {:ok, %Interest{} = interest} <- Base.create_interest(attrs) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", interest_path(conn, :show, interest))
      |> render("show.json-api", data: interest)
    end
  end

  def show(conn, %{"id" => id}) do
    interest = Base.get_interest!(id)
    render(conn, "show.json-api", data: interest)
  end

  def update(conn, %{"data" => %{"id" => id}} = data) do
    interest = Base.get_interest!(id)
    attrs = JaSerializer.Params.to_attributes(data)

    with {:ok, %Interest{} = interest} <- Base.update_interest(interest, attrs) do
      render(conn, "show.json-api", data: interest)
    end
  end

  def delete(conn, %{"id" => id}) do
    interest = Base.get_interest!(id)
    with {:ok, %Interest{}} <- Base.delete_interest(interest) do
      send_resp(conn, :no_content, "")
    end
  end
end
