defmodule ApiWeb.LanguageController do
  use ApiWeb, :controller

  alias Api.Base
  alias Api.Base.Language

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    languages = Base.list_languages()
    render(conn, "index.json-api", data: languages)
  end

  def create(conn, %{"data" => data}) do
    attrs = JaSerializer.Params.to_attributes(data)

    with {:ok, %Language{} = language} <- Base.create_language(attrs) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", language_path(conn, :show, language))
      |> render("show.json-api", data: language)
    end
  end

  def show(conn, %{"id" => id}) do
    language = Base.get_language!(id)
    render(conn, "show.json-api", data: language)
  end

  def update(conn, %{"data" => %{"id" => id}} = data) do
    language = Base.get_language!(id)
    attrs = JaSerializer.Params.to_attributes(data)

    with {:ok, %Language{} = language} <- Base.update_language(language, attrs) do
      render(conn, "show.json-api", data: language)
    end
  end

  def delete(conn, %{"id" => id}) do
    language = Base.get_language!(id)
    with {:ok, %Language{}} <- Base.delete_language(language) do
      send_resp(conn, :no_content, "")
    end
  end
end
