defmodule ApiWeb.ProfileMetaController do
  use ApiWeb, :controller

  import ApiWeb.HttpPlug

  alias Api.Base
  alias Api.Base.ProfileMeta
  alias Api.Repo

  action_fallback ApiWeb.FallbackController

  plug :valid_filters, ~w(profile_id language_id) when action in [:index]

  defp preload(profile_meta) do
    profile_meta
    |> Repo.preload([:profile, :language])
  end

  def index(conn, _params) do
    profile_meta = ProfileMeta
      |> ProfileMeta.filter(conn.assigns.filters)
      |> Repo.all

    render(conn, "index.json-api",
           data: preload(profile_meta),
           opts: [include: conn.params["include"]])
  end

  def create(conn, %{"data" => data, "profile_id" => profile_id}) do
    attrs = JaSerializer.Params.to_attributes(data)
      |> Map.put("profile_id", profile_id)

    case Base.create_profile_meta(attrs) do
      {:ok, %ProfileMeta{} = profile_meta} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location",
                           profile_profile_meta_path(conn,
                                                     :show,
                                                     profile_id,
                                                     profile_meta))
        |> render("show.json-api", data: preload(profile_meta))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(:errors, data: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    profile_meta = ProfileMeta
      |> Repo.get(id)
    render(conn, "show.json-api",
           data: preload(profile_meta),
           opts: [include: conn.params["include"]])
  end

  def update(conn, %{"data" => %{"id" => id} = data, "profile_id" => profile_id}) do
    profile_meta = Base.get_profile_meta!(id)
      |> Repo.preload(:language)
    attrs = JaSerializer.Params.to_attributes(data)
      |> Map.put("profile_id", profile_id)

    case Base.update_profile_meta(profile_meta, attrs) do
      {:ok, %ProfileMeta{} = profile_meta} ->
        render(conn, "show.json-api", data: preload(profile_meta))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(:errors, data: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    profile_meta = Base.get_profile_meta!(id)

    with {:ok, %ProfileMeta{}} <- Base.delete_profile_meta(profile_meta) do
      send_resp(conn, :no_content, "")
    end
  end
end
