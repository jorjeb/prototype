defmodule ApiWeb.CountryController do
  use ApiWeb, :controller

  alias Api.Base

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    countries = Base.list_countries()
    render(conn, "index.json-api", data: countries)
  end

  def show(conn, %{"id" => id}) do
    country = Base.get_country!(id)
    render(conn, "show.json-api", data: country)
  end
end
