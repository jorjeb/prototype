defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json-api"]
    plug JaSerializer.ContentTypeNegotiation
    plug JaSerializer.Deserializer
  end

  pipeline :browser do
    plug :accepts, ["html", "json-api"]
    plug :fetch_session
    plug :fetch_flash
    # plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/api/v1", ApiWeb do
    pipe_through :api

    resources "/languages", LanguageController, except: [:new, :edit]
    resources "/interests", InterestController, except: [:new, :edit]
    resources "/countries", CountryController, only: [:index, :show]

    resources "/profiles", ProfileController, except: [:new, :edit] do
      resources "/profile-meta", ProfileMetaController, except: [:new, :edit]
      resources "/photos", PhotoController, only: [:index, :show, :delete]
      resources "/interests", ProfileMetaController, only: [:index, :show]
    end
  end

  scope "/storage/v1", ApiWeb do
    pipe_through :browser

    resources "/photos", PhotoController, only: [:create]
  end

  # Other scopes may use custom stacks.
  # scope "/api", ApiWeb do
  #   pipe_through :api
  # end
end
