defmodule ApiWeb.HttpPlug do
  import Plug.Conn

  def valid_filters(conn, params) do
    filters = Enum.filter(conn.params, fn({k, _}) ->
      Enum.member?(params, k)
    end)
    conn |> assign(:filters, filters)
  end
end
