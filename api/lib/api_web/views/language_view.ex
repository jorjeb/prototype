defmodule ApiWeb.LanguageView do
  use ApiWeb, :view

  attributes [:name, :code]
end
