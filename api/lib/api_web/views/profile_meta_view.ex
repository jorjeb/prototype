defmodule ApiWeb.ProfileMetaView do
  use ApiWeb, :view

  alias ApiWeb.LanguageView

  attributes [:blurb, :living, :smoking_habits, :drinking_habits]

  has_one :language,
    serializer: LanguageView,
    include: false,
    identifiers: :when_included
end
