defmodule ApiWeb.CountryView do
  use ApiWeb, :view

  attributes [:code, :name]
end
