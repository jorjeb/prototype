defmodule ApiWeb.PhotoView do
  use ApiWeb, :view

  attributes [:uuid]
end
