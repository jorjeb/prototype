defmodule ApiWeb.ProfileView do
  use ApiWeb, :view

  alias ApiWeb.{CountryView, ProfileMetaView, PhotoView, InterestView}

  attributes [
    :display_name,
    :height,
    :weight,
    :hair_colour,
    :eye_colour,
    :body_type,
    :body_art,
    :gender,
    :looking_for,
    :orientation,
    :civil_status,
    :ethnicity,
    :star_sign,
    :religion,
    :education,
    :children
  ]

  has_one :country,
    serializer: CountryView,
    include: false,
    identifiers: :when_included

  has_many :profile_meta,
    serializer: ProfileMetaView,
    include: false,
    identifiers: :when_included

  has_many :photos,
    serializer: PhotoView,
    include: false,
    identifiers: :when_included

  has_many :interests,
    serializer: InterestView,
    include: false,
    identifiers: :when_included
end
