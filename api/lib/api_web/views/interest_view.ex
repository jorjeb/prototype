defmodule ApiWeb.InterestView do
  use ApiWeb, :view

  attributes [:name, :tag, :display_image]
end
