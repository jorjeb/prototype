server {
  server_name prototype.dev;

  listen 80;
  listen [::]:80;

  return 301 https://$host$request_uri;
}

server {
  server_name prototype.dev;

  listen 443;
  listen [::]:443;
  charset utf-8;

  ssl on;
  ssl_certificate /usr/local/etc/ssl/certs/certificate.pem;
  ssl_certificate_key /usr/local/etc/ssl/certs/key.pem;

  client_max_body_size 75M;
  sendfile on;
  keepalive_timeout 0;

  gzip on;
  gzip_comp_level 5;
  gzip_min_length 256;
  gzip_proxied any;
  gzip_vary on;
  gzip_types
    application/atom+xml
    application/javascript
    application/json
    application/ld+json
    application/manifest+json
    application/rss+xml
    application/vnd.geo+json
    application/vnd.ms-fontobject
    application/x-javascript
    application/x-font-ttf
    application/x-web-app-manifest+json
    application/xhtml+xml
    application/xml
    font/opentype
    image/bmp
    image/svg+xml
    image/x-icon
    text/cache-manifest
    text/css
    text/javascript
    text/plain
    text/vcard
    text/vnd.rim.location.xloc
    text/vtt
    text/x-component
    text/x-cross-domain-policy;
  gzip_disable "MSIE [1-6]\.(?!.*SV1)";

  location /uploads {
    root /var/www/storage;
    try_files $uri =404;
  }

  location ~ ^\/(api|storage) {
    proxy_pass http://localhost:4000;

    proxy_redirect off;

    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }

  location / {
    root /var/www/web;
    try_files $uri $uri/ /index.html;
  }
}
