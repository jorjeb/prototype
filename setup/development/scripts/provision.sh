#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

# Update Package List

apt-get update

# Update System Packages

apt-get -y upgrade

# Force Locale

echo "LC_ALL=en_US.UTF-8" >> /etc/default/locale
locale-gen en_US.UTF-8

# Install Curl

apt-get install -y curl

# Install Some PPAs

add-apt-repository ppa:adiscon/v8-stable -y

# Update Package Lists

apt-get update

# Install Some Basic Packages

apt-get install -y \
  apg \
  build-essential \
  ca-certificates \
  cachefilesd \
  checkinstall \
  dos2unix \
  git \
  libbz2-dev \
  libffi-dev \
  libssl-dev \
  libpcre3 \
  libpcre3-dev \
  libpq-dev \
  rsyslog \
  wget \
  zlib1g-dev

# Install Cachefilesd To Speed Up NFS Mount

apt-get install -y cachefilesd
echo "RUN=yes" > /etc/default/cachefilesd

# Set My Timezone

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Install Nginx

apt-get install -y nginx
rm -f /etc/nginx/sites-enabled/*
rm -f /etc/nginx/sites-available/*
sed -i "s/# server_names_hash_bucket_size.*/server_names_hash_bucket_size 64;/" /etc/nginx/nginx.conf
mv /tmp/api.conf /etc/nginx/conf.d
systemctl restart nginx
cd /var/www
chown -R www-data: html
chmod -R g+w html

# Add Vagrant User To WWW-Data

usermod -a -G www-data vagrant
id vagrant
groups vagrant

# Create necessary directories

cd /var/www
mkdir logs
chown -R www-data: logs
chmod -R g+w logs
mkdir run
chown -R www-data: run
chmod -R g+w run

# Install Postgres

apt-get install -y postgresql
pg_version=`pg_config --version | awk '{print $2}' | sed -e 's/.[0-9]$//'`

# Set Timezone To UTC

sed -i "s/#timezone = 'GMT'/timezone = 'UTC'/g" /etc/postgresql/$pg_version/main/postgresql.conf

# Configure Postgres Remote Access

sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/$pg_version/main/postgresql.conf
echo "host    all             all             10.0.2.2/32               md5" | tee -a /etc/postgresql/$pg_version/main/pg_hba.conf

# Create Postgres Role

db_pass=`apg -a 1 -m 63 -E "\\\`'\"/&" -n 1`
sudo -u postgres psql -c "CREATE ROLE baseup LOGIN UNENCRYPTED PASSWORD '"$db_pass"' SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;"
sudo -u postgres /usr/bin/createdb --echo --owner=baseup baseup_db
systemctl restart postgresql

# Install Python 3.6

cd /usr/src
wget https://www.python.org/ftp/python/3.6.1/Python-3.6.1.tgz
if ! [ `md5sum Python-3.6.1.tgz | cut -c 1-32` == '2d0fc9f3a5940707590e07f03ecb08b9' ]
then
  echo 'invalid MD5 checksum'
  exit 1;
fi
tar -xzf Python-3.6.1.tgz
cd Python-3.6.1
./configure --enable-shared # production flags: --enable-optimizations
make altinstall
ldconfig

# Setup and activate virtualenv

cd /var/www/api
python3.6 -m pip install virtualenv
virtualenv -p `which python3.6` venv
source ./venv/bin/activate

# Install Dependencies

pip3.6 install -r requirements.txt

# Install psycopg2

cd /usr/src
wget http://initd.org/psycopg/tarballs/PSYCOPG-2-7/psycopg2-2.7.1.tar.gz
if ! [ `sha256sum psycopg2-2.7.1.tar.gz | cut -c 1-64` == '86c9355f5374b008c8479bc00023b295c07d508f7c3b91dbd2e74f8925b1d9c6' ]
then
  echo 'invalid SHA256 checksum'
  exit 1;
fi
tar -xzf psycopg2-2.7.1.tar.gz
cd psycopg2-2.7.1
python3.6 setup.py build
python3.6 setup.py install

# Install Node and npm

curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get install -y nodejs

# Build project

cd /var/www/api
python3.6 manage.py migrate --noinput
python3.6 manage.py collectstatic --noinput

cd /var/www/web
npm install

# Deactivate virtualenv

deactivate

# Create .env file

cp -f .env.sample .env
chown www-data: .env
sed -i 's/DOMAIN=baseup.co/DOMAIN=baseup.dev/' .env
sed -i 's/DEBUG=False/DEBUG=True/' .env
secret_key=`apg -a 1 -m 63 -E "\\\`'\"/&" -n 1`
sed -i 's/SECRET_KEY=$/SECRET_KEY="'$secret_key'"/' .env
sed -i 's/POSTGRES_DB=baseup/POSTGRES_DB=baseup_db/' .env
sed -i 's/POSTGRES_USER=/POSTGRES_USER=baseup/' .env
sed -i 's/POSTGRES_PASSWORD=$/POSTGRES_PASSWORD="'$db_pass'"/' .env

# Start REST API service at boot

mv /tmp/api.service /etc/systemd/system
systemctl enable api
systemctl start api

# Enable RabbitMQ Application Repository

echo "deb http://www.rabbitmq.com/debian/ testing main" | tee /etc/apt/sources.list.d/rabbitmq.list
curl https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | apt-key add -

# Update Package List

apt-get update

# Install RabbitMQ

apt-get install -y rabbitmq-server

# Install Redis

apt-get install -y redis-server

# Install Memcached

apt-get install -y memcached

# Tell Syslog About Our Local7 Facility

cd /var/log
echo "local7.* /var/log/baseup.log" >> /etc/rsyslog.d/30-baseup.conf
echo "& ~" >> /etc/rsyslog.d/30-baseup.conf
touch baseup.log
chown syslog:adm baseup.log

# Restart Syslog

systemctl restart rsyslog

# Clean Up

apt-get -y autoremove
apt-get -y clean

# Enable Swap Memory

/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
/sbin/mkswap /var/swap.1
/sbin/swapon /var/swap.1

# Minimize The Disk Image

echo "Minimizing disk image..."
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY
sync
